"""
Converts the markdown files into static HTML pages
"""
import os

import markdown

with open(f"{os.getcwd()}{os.sep}html{os.sep}template.html") as file:
    template = file.read()

links = [
    f"https://addgaming.gitlab.io/10minutepython/{file[:-3]}" for file in os.listdir(f"{os.getcwd()}{os.sep}markdown")
]

chapters = [
    f"<li><a class='list-group-item list-group-item-action chapter-list-item' href='{link}'>"
    f"{' '.join(file.split('-')[:-1]).replace('_', '.')}</a></li>"
    for file, link in zip(os.listdir(f"{os.getcwd()}{os.sep}markdown"), links)
]
chapters = "\n".join(chapters)
chapters = '<ul class="list-group list-group-flush" style="background-color: transparent;">\n' \
           + chapters \
           + "\n</ul>"

# generate individual pages
for file in os.listdir(f"{os.getcwd()}{os.sep}markdown"):
    with open(f"{os.getcwd()}{os.sep}markdown{os.sep}{file}", "r", encoding="utf8") as md:
        chapter = markdown.markdown(md.read(), extensions=["extra", "codehilite"])

    chapter = "<div class='px-5 py-3 text-left text-bg-dark'>" + chapter + "</div>"

    with open(f"{os.getcwd()}{os.sep}html{os.sep}chapter_nav.html", "r", encoding="utf8") as nav:
        navigation = nav.read()

    html = navigation + "\n" + chapter

    with open(f"{os.getcwd()}{os.sep}public{os.sep}{file[:-3]}.html", "w", encoding="utf8") as target:
        target.write(
            template.replace("TEMPLATE_BODY", html).
            replace("LANGUAGE", "de").
            replace("TITLE", ' '.join(file.split('-')[:-1]).replace('_', '.')).
            replace("TEMPLATE_HEAD", "<link href='chapters.css' rel='stylesheet'>").
            replace("BODY_CLASS", '').
            replace("HTML_CLASS", '')
        )

# generate index
with open(f"{os.getcwd()}{os.sep}html{os.sep}greeting.html") as html:
    body = html.read().replace("CHAPTERS", chapters)

with open(f"{os.getcwd()}{os.sep}public{os.sep}index.html", "w") as page:
    page.write(
        template.replace("TEMPLATE_BODY", body).
        replace("LANGUAGE", "de").
        replace("TITLE", "Home").
        replace("TEMPLATE_HEAD", "<link href='cover.css' rel='stylesheet'>").
        replace("BODY_CLASS", 'class="d-flex h-100 text-center text-bg-dark"').
        replace("HTML_CLASS", 'class="h-100"')
    )
