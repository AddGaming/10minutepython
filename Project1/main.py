"""
Starting Point of the CLI
Programm to import and de-duplicate files from an external device
"""
import os
from CLI import registered


if __name__ == "__main__":
    os.chdir(os.sep.join(__file__.split(os.sep)[:-1]))

    while True:
        cmd_param = input("$: ")
        if cmd_param.split()[0] in registered:
            registered[cmd_param.split()[0]](*cmd_param.split()[1:])
        else:
            print(f"unrecognized input: {cmd_param.split()[0]}")
