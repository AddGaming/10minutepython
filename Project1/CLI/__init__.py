"""
Contains the functions for the CLI.
This file contains the mapping and administration of these functions
"""
from .settings import Settings

SETTINGS = Settings()

from .cli_functions import copy_from, print_tree


# is defined in here for simplicity.
def give_help() -> None:
    """
    prints the documentation of all registered functions to the screen.
    :return: None
    """
    for key, val in registered.items():
        print(f"{key}: {val.__doc__}")


# register command for cli
registered = {
    "import": copy_from,
    "tree": print_tree,
    "help": give_help,
    "quit": quit,
    "exit": quit,
    # "cls": clear,
    # "clear": clear,
    "settings": SETTINGS.set,
}

if __name__ == "__main__":
    ...
