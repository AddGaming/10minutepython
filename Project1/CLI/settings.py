"""
Settings Class for settings of Application
"""
from __future__ import annotations

import json
import os

from jsonschema import validate, exceptions


class Settings:
    """
    Class for handling user set settings management
    """
    default_source: str
    default_destination: str
    file_format_list: [str]

    def __init__(self, default_source: str = "", default_destination: str = "", file_format_list: [str] = None):
        if not default_source:
            default_source = os.sep.join(__file__.split(os.sep)[:-1])
        if not default_destination:
            default_destination = os.sep.join(__file__.split(os.sep)[:-1])
        if not file_format_list:
            file_format_list = [".png", ".jpg"]

        self.default_source = default_source
        self.file_format_list = file_format_list
        self.default_destination = default_destination

    @staticmethod
    def _schema() -> dict:
        """
        The validation schema
        :return: Json-Validation-Schema
        """
        return {
            "type": "object",
            "properties": {
                "default_source": {"type": "string"},
                "default_destination": {"type": "string"},
                "file_format_list": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                }
            },
            "required": [
                "default_source",
                "default_destination",
                "file_format_list"
            ]
        }

    @classmethod
    def __new_save(cls) -> Settings:
        """
        Saves a new settings.json
        :return: the new Settings object
        """
        c = cls()
        c.save_to_file()
        return c

    @classmethod
    def load_from_file(cls) -> Settings:
        """
        loads the settings from the save file
        :return: Settings
        """
        path = os.sep.join(__file__.split(os.sep)[:-1])
        try:
            with open(f'{path}{os.sep}settings.json', "r") as f:
                # no validation needed, because unaccepted input is caught in loads() and __init__()
                content = json.loads(f.read())
        except IOError:
            print("No settings found\n\t-> booting from blank defaults...")
            return cls.__new_save()
        except json.decoder.JSONDecodeError:
            print("settings.json is corrupt\n\t-> instantiating a new version...")
            return cls.__new_save()
        try:
            validate(content, cls._schema())
        except exceptions.ValidationError:
            print("settings.json is invalid\n\t-> instantiating a new version...")
            return cls.__new_save()
        return cls(
            default_source=content['default_source'],
            default_destination=content['default_destination'],
            file_format_list=content['file_format_list']
        )

    def save_to_file(self) -> None:
        """
        saves the settings to the save file
        :return: None
        """
        dump = json.dumps(self.__dict__)
        path = os.sep.join(__file__.split(os.sep)[:-1])
        with open(f'{path}{os.sep}settings.json', "w") as f:
            f.write(dump)

    def set(self, *args) -> None:
        """
        Sets the param_name to the value of param.
        If no args are passed on, it instead returns the current settings
        :param param_name: str - the name of the attribute you want to edit
        :param value: str - the value you want it to set to
        :return: None
        """
        if not args:
            for key, val in self.__dict__.items():
                print(f"\t{key}: {val}")
            return

        if len(args) == 1:
            try:
                print(f"\t{args[0]}: {getattr(self, args[0])}")
                print("If you want to change the Parameter, use 'settings <parameter> <new_value>'")
            except AttributeError:
                print(f"\tAttribute '{args[0]}' doesn't exists")
        elif len(args) == 2:
            if args[0] == "default_source" or args[0] == "default_destination":
                if not os.path.isdir(args[1]):
                    print(f"\tThe path you entered is not valid.\n\tpath: {args[1]}")
                    return
            try:
                getattr(self, args[0])
                setattr(self, args[0], args[1])
            except AttributeError:
                print(f"\tAttribute '{args[0]}' doesn't exists")
        else:
            print(f"\tTo many arguments: {args}")

        self.save_to_file()
