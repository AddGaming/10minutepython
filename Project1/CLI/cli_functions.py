"""
The functions for the CLI
"""
import os
import shutil

from . import SETTINGS


def copy_from(source: str = "", destination: str = "", endings: [str] = None) -> None:
    """
    Copies all elements fitting the endings descriptor into a destination folder.
    Traverses subdirectories recursively.
    :param source: str - source path
    :param destination: str - destination path
    :param endings: [str] - list of endings
    :raises RuntimeError: if source or destination are not folders
    :return: None
    """
    if not source:
        source = SETTINGS.default_source
    if not destination:
        destination = SETTINGS.default_destination
    if not endings:
        endings = SETTINGS.file_format_list

    if not os.path.isdir(source) or not os.path.isdir(destination):
        raise RuntimeError("The source path is not a folder")

    destination_list = os.listdir(destination)

    for element in os.listdir(source):
        element_path = f"{source}{os.sep}{element}"
        if os.path.isdir(element_path):
            copy_from(element_path, destination, endings)
        else:
            if element in destination_list:
                continue
            for ending in endings:
                if element.endswith(ending):
                    shutil.copy2(element_path, f"{destination}{os.sep}{element}")
                    break


def print_tree(path: str = "", depth: int = 1) -> None:
    """
    prints the structure of a given folder | if path leads to file, only file path is printed
    :param path: str - the path
    :param depth: int - current recursion depth
    :return: None
    """
    if not path:
        path = SETTINGS.default_source

    if os.path.isdir(path):
        for element in os.listdir(path):
            print(depth * "|--" + element)
            element_path = f"{path}{os.sep}{element}"
            if os.path.isdir(element_path):
                print_tree(element_path, depth=depth + 1)
    else:
        print(path)


if __name__ == "__main__":
    ...
