[![pipeline status](https://gitlab.com/AddGaming/10minutepython/badges/main/pipeline.svg)](https://gitlab.com/AddGaming/10minutepython/-/commits/main)

Repository for my "Book"/Tutorial page: [10minutepython](https://addgaming.gitlab.io/10minutepython)

Styles from https://github.com/richleland/pygments-css
and Bootstrap

Work in progress:
_________________________________

# Einleitung

Dies ist ein kleiner Kurs zum Thema Alltagsprogrammierung.    
Vielleicht erweitre ich das ganze auch auf Informatik generell, aber für den Moment bleibe ich bei Programmierung.    
Ich plane alles so einfach zu halten, dass wirklich jeder, der mitmachen will, auch mitmachen kann.    
Keine Vorerfahrung mit Computern vorausgesetzt!    
Da ich weiß, dass das Leben stressig sein kann und man auch mal Feierabend will und nicht Abendschule,   
werde ich jede Lektion in einem 10-Minuten Rahmen halten.   
Alles sollte also in unter 10 Minuten gelesen und erledigt werden können,   
was hoffentlich motiviert auch täglich mitzumachen.

# Inhaltsverzeichnis

1. Grundlagen
    1. Hi Mom
    2. Was sind Funktionen und Variablen?
    3. Iteration und Datenmengen
        1. Listen & Tupel
        2. Dictionaries & Sets
        3. Comprehension
    4. Typen und Operatoren
        1. Wahrheitswerte & if-else
        2. Zahlen
        3. Text
    5. Klassen
    6. Files
        1. Programme speichern und effektiv entwickeln
        2. Mehrere Files und imports
        3. Packaging
    7. Entwicklungspraktiken
        1. Virtual Environments
        2. Testing
        3. GIT (is) good
        4. English is the new German & Kommentare
        5. Wann sollte ich programmieren?
        6. Mark it down
2. Real live apps
    1. Sauber Importieren von Files
        1. Main Loop
        2. Py.Os
        3. Rekursion
        4. Kopieren
        5. Refactor - Programm Strukturierung und Separierung
        6. Ein- und Auspacken & Documentation
        7. JSON
        8. Try, Except, Try, Except, Try, ...
        9. Klassen- & Statische Methoden
3. Wie gehts weiter?
    1. Youtube empfehlungen
    2. Buch empfehlungen

# TODO:

____________________

# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

- [ ] Settings
- [x] Try except
- [x] decorators lite
- [x] getattr setattr
- [x] action!
- [ ] tests
- [ ] utils

## Kapitel 2.2: Aufräumen von Files

- [ ] Duplikate finden
- [ ] Ordner nach...
    - [ ] File typ
    - [ ] Datum
    - [ ] Andere Meta daten
- [ ] Zippen
- ...

## Kapitel 2.3: Repetitive Bildbearbeitung

## Kapitel 2.4: Eigenen Blog erstellen

Markdown
gitlab pages

## Kapitel 2.4: Videospiele durchrechnen

## Kapitel 2.5: Ausgaben analysieren

## Kapitel 2.6: Gesünder ernähren

## Kapitel 2.7: Anti Twitter Cancle Protection

# Kapitel 4: Lösungen
