# Kapitel 1: Grundlagen

## Kapitel 1.3: Iteration und Datenmengen

### Kapitel 1.3.3: Comprehension

Das heutige Thema ist List comprehension.   
*List comprehension* ermöglicht es uns *kompakt und kurz* neue *iterierbare Datenkonstrukte* zu generieren.

Auch hier sollte ein Beispiel wieder verständlicher sein als eine Beschreibung.    
Bisher haben wir *for-loops* und *Listen* kennen gelernt.    
Wenn wir nun also eine Liste mit den Zahlen 0 bis 99 erstellen wollen, würden wir das auf folgende Weise machen:    

```python
>>> meine_liste = list()
>>> for x in range(100):
...   meine_liste.append(x)
...
>>> meine_liste
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ..., 98, 99]
```

`range()` ist eine Funktion, welche uns keine Liste oder der gleichen zurückgibt, sondern ist ein *Generator*.    
Wenn wir das Ergebnis von `range()` in einer Variabel zwischenspeichern und uns anschauen, sehen wir, dass es keine uns bekannte Datenstruktur ist.   

```python
>>> a = range(10)
>>> a
range(0, 10)
```

Die Funktion eine Generator ist lediglich, dass wir über ihn iterieren können.   
In diesem Fall nutzen wir den `range()` Iterator um über die Zahlen 0 bis *n* zu iterieren.   
Dabei ist *n* eine beliebige natürliche Zahl.

> **Aufgabe**:    
> Schreibe eine *for-Schleife* welche die Zahlen 0 bis -10 in eine Liste schreibt.
>
> Tipp: `*` multipliziert, `+` addiert, `-` subtrahiert und `/` dividiert und `()` klammern eine Rechnung.    
> Punk vor Strich und Klammer Regeln werden beim Ausrechnen in Python natürlich beachtet. 

Mit einer Listcomprehension können wir das auch eine Liste mit den Zahlen 0 bis 99 generieren, aber in nur einer Zeile Code!

```python
>>> meine_liste = [x for x in range(100)]
>>> meine_liste
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ..., 98, 99]
```

Eine List Comprehension lässt sich in 3 Teile unterteilen:

1. Der Ausdruck, der bei der Auswertung ausgeführt wird. `[`*x*` for x in range(100)]`
2. Der For-loop. Dieser Teil sollte bereits bekannt sein. `[x `*for x in* `range(100)]`
3. Etwas Iterierbares. In diesem Beispiel `range()`. `[x for x in `*range(100)*`]`

Wir können aber natürlich auch die bereits uns bekannten Listen, Tuple, Sets und Dictionaries anstatt eines *Generators* benutzen.    
Hier rechnen wir das Quadrat zu jedem Element in `meine_liste` aus und schreiben die resultierende Liste in eine neue Variabel.

```python
>>> quadrate = [x*x for x in meine_liste]
>>> quadrate
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, ..., 9604, 9801]
>>> meine_liste
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ..., 98, 99]
```

Wir können *Comprehensions* auch Sets und Tupel generieren lassen:

```python
>>> meine_liste = tuple(x for x in range(100))
>>> meine_liste
(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ..., 98, 99)
>>> meine_liste = {x for x in range(100)}
>>> meine_liste
{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ..., 98, 99}
```

Warum sollte man nun also die kompaktere Schreibweise benutzen?

- Kompakter heist oft verständlicher.
- Dazu kommt, je weniger Code wir schreiben, desto weniger Platz haben Fehler um sich einzuschleichen.
- Laufzeit ist zwar für uns irrelevant, aber *Comprehensions* sind auch schneller als die explizite Schreibweise.
- Wenn wir uns mit statischen Datenstrukturen wie Tupel beschäftigen,
  sind for-loops wie im erster Beispiel nicht möglich
  und Comprehension der einzige Weg iterativ eine solche Datenstruktur zu befüllen.

Schluss endlich würde ich jedoch empfehlen Code zu schreiben, den du verstehst.   
Wenn du also die normalen *for-loops* lesbarer findest, benutze diese.    
Ich werde aber manchmal *Comprehensions* nutzen, um Code kompakter zu machen.    
Daher habe ich sie hier vorgestellt.

> **Aufgabe**:    
> Generiere jeweils ein Tupel mit der beschriebenen Zahlenfolge indem du die die Liste 
> `[0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10]` als Ausgangspunk nutzt:  
> - `( 0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100)`
> - `(10, 9, 8, 7,  6,  5,  4,  3,  2,  1,   0)`
>
> Tipp: `*` multipliziert, `+` addiert, `-` subrahiert und `/` dividiert und `()` klammern eine Rechnung.    
> Punk vor Strich und Klammer Regeln werden beim Ausrechnen in Python natürlich beachtet. 

> **Bonus Aufgabe** (schwer):
> Analog zu der Aufgabe obendrüber. Zieltupel: `(50, 32, 18, 8, 2, 0, 2, 8, 18, 32, 50)`   

