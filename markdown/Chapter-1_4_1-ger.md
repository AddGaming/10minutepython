# Kapitel 1: Grundlagen

## Kapitel 1.4: Typen und Operatoren

### Kapitel 1.4.1: Wahrheitswerte & if-else

Bisher habe ich Typen nicht erklärt und ein intuitives Verständnis vorausgesetzt.    
Dies ändert sich in diesem Kapitel.    
Ich werde die 3 Haupttypen näher beleuchten und wir werden in Kapitel 1.5 auch sehen, wie sie unter der Haube funktionieren.    
Heute fangen wir mit den einfachsten der 3 an.   

*Boolean* (oder abgekürzt *bool*) ist der Name für Wahrheitswerte.    
Diese können nur zwei Werte haben: Wahr (*True*) und Falsch (*False*).   

```python
wahr = True
falsch = False
```

An sich wars das auch schon.   
Aber Wahrheitswerte sind nicht deshalb interessant, weil sie komplex sind,    
sondern weil wir mit ihnen komplexe Sachen machen können.   
*Wenn etwas wahr ist, mache etwas anderes.*; Wenn-dann Logik.    
Oder in Python:

```python
if condition:
  # do stuff
else:
  # do stuff 
```

Nun können wir noch Wahrheitswerte verrechnen und manipulieren 

| Operation  | Example                                                    | Description                                                                      |
|------------|------------------------------------------------------------|----------------------------------------------------------------------------------|
| negieren   | `not True` -> `False`                                      | Invertiert den Wahrheitswert                                                     |
| und        | `True and False` -> `False`<br>`True and True` -> `True`   | Vereinigt die Wahrheitswerte. Wird nur wahr, wenn beide Teile wahr sind.         |
| oder       | `True and False` -> `True`<br>`False and False` -> `False` | Vereinigt die Wahrheitswerte. Wird wahr, wenn einer der beiden Teile wahr ist.   |
| gleichheit | `1 == 1` -> `True`                                         | Vergleicht die beiden Werte. Ist wahr, wenn beide gleich sind. Ansonsten falsch. |

Den Nutzen illustriere ich kurz an einem Beispiel:

```python
>>> liste1 = [1,2,3,4, 5, 6, 7, 8, 9,10]
>>> liste2 = [6,7,8,9,10,11,12,13,14,15]
>>> for element in liste1:
...   if element in liste2:
...     print(element)
...
6
7
8
9
10
```

Hier iterieren wir über die erste Liste. Für jedes Element überprüfen wir, ob es in der zweiten Liste ist.   
Falls ja, geben wir das Element auf der Consol aus.    

Ein anderes Beispiel was häufig in Code vorkommt ist Verhaltensentscheidung basierend auf den Wert einer Variabel.   
Auch hier klingt der Satz schwieriger als das Beispiel:

```python
def ist_giftig(wort):
  if wort == "Fingerhut":
    return True
  if wort == "Fliegenpilz":
    return True
  if wort == "Milch":
    return False
  if wort == "Johannisbeere":
    return False
```

Das hier ist eine naive Implementierung einer Funktion `ist_giftig()` welche ein Wort erhält und schaut, ob das Wort als Giftig erkannt wird.    

> **Aufgabe**:
> Erkennst du das Problem bei der vorgeschlagenen Implementierung von `ist_giftig()`?
> Fällt dir eine Verbesserung für diese Funktion ein?

Antwort:    
Das Problem ist, was passiert, wenn wir ein Wort eingeben, was die Funktion nicht kennt?
Schauen wir uns das ganze mal in der Consol an:

```python
>>> a = ist_giftig("Sauerampfer")
>>> a
>>> 
```

Huh? Was ist denn da passiert? Da ist ja überhaupt kein Wert!    
Ok, dann machen wir es eben explizit:

```python
>>> print(a)
None
```

Und Tatsache. `a` hat einfach keinen Wert.    
Das kann in einem größeren Programm schonmal zu Fehlern führen, wenn eine Variabel keinen Wert hat.   
Also passen wir die Funktion an:

```python
def ist_giftig(wort):
  if wort == "Fingerhut":
    return True
  if wort == "Fliegenpilz":
    return True
  else:
    return False
```

Jetzt wir immer ein Wert zurückgegeben.    
Wenn die Funktion das Wort nicht kennt, sagt sie einfach, dass es mal nicht giftig ist.   
Ob das ein gutes Verhalten in der echten Welt wäre, sei mal dahingestellt,    
aber programmatisch ist dies wesentlich besser, da nun immer ein Wahrheitswert zurückgegeben wird.

Nun, wir haben noch eine letzte Eigenheit in Python, die ich erwähnen sollte.    
Wir können auch nicht-Wahrheitswerte in Wahrheitswerte konvertieren.    
Das geht mit der `bool()` Funktion.

> **Aufgabe**:
> Probiere folgende Konvertierungen auf deiner Consol aus und überlege dir, ob du eine Regelmäßigkeit in der Konvertierung erkennen kannst.   
> Du kannst den Code einfach Kopieren und in deiner Consol einfügen.    
> ```python
> bool(True)
> bool(False)
> bool("True")
> bool("False")
> bool("hahaha")
> bool("")
> bool(0)
> bool(1)
> bool(4)
> bool([])
> bool([1,2,3])
> bool([True, True, True])
> ```

Ich löse dann mal auf:     
Python konvertiert leere Dinge zu `False` und gefüllte Dinge zu `True`.    
`""` ist ein leerer Text und somit `False`.    
`"False"` ist ein gefüllte Text und somit `True`.    
Das funktioniert genauso für Listen.    
Bei Zahlen wurde definiert, dass die `0` die Rolle des Leeren-Elementes einnimmt.    
Also ist `bool(0)` `False` und jede andere Zahl `True`.    

Und damit ist auch schon fast alles Relevante zu Bools erklärt.     
Leider macht hier nur Übung den Meister.    
Daher hier noch zwei weitere Aufgaben.    

> **Aufgabe**:
> Schreibe eine Funktion, welche zwei Listen nimmt und `False` ausgibt, wenn sie kein Element teilen und `True`, wenn sie mindestens ein Element teilen.     
> Beispielaufrufe:    
> `share_element([0,1,2,3],[3,4,5])` -> `True`    
> `share_element(["a","b","c"],[3,4,5])` -> `False`    
> `share_element(["a","b","c"],["c","d","e"])` -> `True`   

> **Aufgabe**:
> Zähle, wie oft der Wert `0` in der folgenden Liste vorkommt:
> `[0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1]`

