# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

### Kapitel 2.1.5: Refactor - Programm Strukturierung und Separierung

Ich habe zwar gesagt, dass wir die Funktionalität benutzbar machen, aber wahrscheinlich kannst du das schon selbst.
Der Code, um dieses zu erreichen ist folgender:

```python
...

if __name__ == "__main__":
    os.chdir(os.sep.join(__file__.split(os.sep)[:-1]))

    while True:
        cmd_param = input("$: ")
        if cmd_param.split()[0] == "quit":
            quit()
        if cmd_param.split()[0] == "tree":
            print_tree(cmd_param.split()[1])
        if cmd_param.split()[0] == "copy":
            copy_from(cmd_param.split()[1], cmd_param.split()[2], [".png"])
        else:
            print(f"unrecognized input: {cmd_param.split()[0]}")
```

Gehen wir einen beispielhaften Befehl einmal Schritt für Schritt durch:
`$: copy c:\\mein\\pfad c:\\mein\\anderer\\pfad`.
Dieser wird mit `split()` geteilt. Solange `split()` keinen Parameter bekommt, nimmt die funktion das Leerzeichen
als Standardwert an.

```
$: copy c:\\mein\\pfad c:\\mein\\anderer\\pfad
-split-> [copy, c:\\mein\\pfad, c:\\mein\\anderer\\pfad]
```

Nun landen wir in den `if`-Fall, indem das erste Element in der Liste "copy" ist.
Dort rufen wir nun die Funktion `copy_from` mit dem zweiten und dritten Element der Liste auf.

```
$: copy c:\\mein\\pfad c:\\mein\\anderer\\pfad
-split-> ["copy", "c:\\mein\\pfad", "c:\\mein\\anderer\\pfad"]
-> copy_from("c:\\mein\\pfad", "c:\\mein\\anderer\\pfad", [".png"])
```

Wie die Funktion `copy_from` funktioniert, haben wir bereits im letzten Kapitel gelernt.

Aber was hier auffällt, ist dass wir wiederholt den selben Code schreiben.
Wir teilen in jeden `if` aufs neue unsere Eingabe, und setzten danach die anderen Parameter dieser Liste ein.
Aber wir schreiben Programme, damit wir nicht immer wieder das selbe machen müssen.
Deshalb haben wir Schleifen und Rekursion und Variablen.
Also schreiben wir heute das Programm so um, dass wir uns nicht mehr immer wiederholen müssen
und es dennoch einfach bleibt, neue Funktionen hinzuzufügen.

Als erstes legen wir uns dafür ein Paar neue Dateien an.
Wir brauchen ein neues Package mit dem Namen "CLI" in welchem wir unsere Funktionen
für unsere Kommando-Konsolen-Bedienbare Application (**C**ommand-**L**ine-**I**nterface Application) oder CLI in kurz,
ablegen werden.
In dem Package befindet sich eine `__init__.py`, `cli_functions.py` und eine `settings.py`.

```
main.py
CLI
 |-- __init__.py
 |-- cli_functions.py
 |-- settings.py 
```                                                              

Fange wir an mit dem einfachsten Refactor (de. ~ Umstrukturierung) den wir heute machen werden.
Wir kopieren alle Funktionen nach `cli_functions.py`

**cli_functions.py**

```python
"""
The functions for the CLI
"""
import os
import shutil


def copy_from(source: str, destination: str, endings: [str]) -> None:
    ...


def print_tree(path: str, depth: int = 1) -> None:
    ...
```

Nun also zu der `__init__.py`.
In dieser werden wir dem Namen entsprechend die Initialisierung durchführen.

**__init__.py**

```python
"""
Contains the functions for the CLI.
This file contains the mapping and administration of these functions
"""

from .cli_functions import copy_from, print_tree

# register command for cli
registered = {
    "import": copy_from,
    "tree": print_tree,
    "quit": quit,
    "exit": quit,
}
```

Ich habe in dem Kapitel zu Klassen zwar nur in einem Nebensatz erwähnt, dass in Python alles ein Objekt ist,
aber hier benutze ich als erstes mal diese Eigenschaft in einem Programm.
In diesem Dictionary werden Strings als Schlüssel benutzt und Funktionen sind die Werte.
Das Funktionen auch nur Objekte sind, können wir in der Konsole schnell testen:

```python
>>> def test_fun():
...   print("my test")
...
>>> a = test_fun
>>> a
<function test_fun at 0x000001A579CF7920>
>>> test_fun()
my test
>>> a()
my test
```

Am besten spielst du in deinem Interpreter etwas herum um dich mit dem Konzept von Funktionsobjekten vertraut zu machen.
Schließen wir nun den Refactor damit ab, unsere `main.py` anzupassen.

**main.py**

```python
"""
Starting Point of the CLI
Programm to import and de-duplicate files from an external device
"""
import os
from CLI import registered


if __name__ == "__main__":
    os.chdir(os.sep.join(__file__.split(os.sep)[:-1]))

    while True:
        cmd_param = input("$: ")
        if cmd_param.split()[0] in registered:
            registered[cmd_param.split()[0]](*cmd_param.split()[1:])
        else:
            print(f"unrecognized input: {cmd_param.split()[0]}")

```

Dadurch, dass wir unsere Funktionen jetzt in einem Dictionary haben, können wir auch das Dictionary nutzen,
um dynamisch eine Funktion auszuwählen.
Im nächsten Kapitel gehe ich näher darauf ein,
was der `*` in der Zeile `registered[cmd_param.split()[0]](*cmd_param.split()[1:])` macht.

Fassen wir aber für heute erst einmal zusammen, was wir gemacht haben, warum, und wie es funktioniert hat.

#### Zusammenfassung

Wir haben festgestellt, dass wir uns mit jedem neuen If-Statement stark wiederholt hätten und viel code ohne
tatsächlichen nutzen geschrieben hätten.
Um das zu umgehen haben wir alle Funktionen nun in ein Dictionary geschrieben.
Die Kommandos mit dehnen die Befehle genutzt werden ist der Schlüssel.

Da wir auch erkennen konnten, dass sich noch mehrere Funktionen dazugesellen werden, haben wir diese in ein neues File
ausgelagert.
Jetzt hat jede Datei genau eine Aufgabe und wir wissen, wo wir nachschauen müssen um etwas zu ändern.
In der *Main* wird der *Main-Loop* verwaltet, in der *CLI-init* werden die Kommandos den Funktionen zugeordnet,
und in *CLI-functions* sind die ... CLI-Funktionen.
Die Einstellungen für unsere Anwendungen werden in *Settings* später ein neues Zuhause finden.

Der Vorteil daran, dass wir die Zuordnung nun in dem Dictionary haben, ist schnell und einfach synonyme zu definieren.
Wenn wir uns nochmal das Dictionary anschauen...

```python
registered = {
    "import": copy_from,
    "tree": print_tree,
    "quit": quit,
    "exit": quit,
}
```

... stellen wir fest, dass "quit" und "exit" beide die selbe funktion haben.
Wenn wir also später verschiedene Funktionen haben, welche unter verschiedenen Namen verfügbar sein sollen,
können wir das nun einfach erreichen.
Auch wenn du in 1 Monat wieder diese App startest und nicht mehr genau weißt,
ob du nun das beenden als "quit" oder "exit" eingespeichert hattest, wirst du dich freuen, dass beides Funktioniert.

Wie bereits angekündigt wenden wir uns im Nächsten Kapitel dem "*" Operator zu
und schauen uns einen weiteren Vorteil an, den das Strukturieren in ein Dictionary bietet.