# Kapitel 1: Grundlagen

## Kapitel 1.4: Typen und Operatoren

### Kapitel 1.4.2: Text

In diesem Kapitel versuche ich einen hilfreichen Überblick über Text zu geben 
und wie dieser in Programmen repräsentiert ist.   
In diesem Kapitel ist also mehr Python spezifisch als üblich, aber ich versuche es generell zu halten.    

Generell ist Text eine Aneinanderreihung von Zeichen (eng. `character` oder kurz `char`).    
Wenn wir also in Python die Zeichenkette (`string`) `"hallo"` haben,     
ist das in den meisten Programmiersprachen identisch zu: `["h","a","l","l","o"]`.

Das erklärt auch das folgende Verhalten in Python:

```python
>>> for char in "hallo":
...   print(char)
...
h
a
l
l
o
```

Wir iterieren hierbei über jedes Zeichen in `hallo` als wäre der String eine Liste.   
Zeichen sind nun jedoch nicht alle gleich.   
Es gibt eine Klasse von Zeichen die einfach Historisch als erstes in Computer einprogrammiert worden sind.    
Diese Zeichen werden *ASCII*-Zeichen genannt.    
Python kann uns die einfach ausgeben:

```python
>>> import string
>>> string.printable
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ \t\n\r\x0b\x0c'
```

Wie auffällt, fehlen dort eine Menge Buchstaben. Wo sind *é*, *ä*, oder *ê*?   
Damit wir diese Zeichen nutzen können müssen wir einen anderen Zeichensatz verwenden.   
ASCII ist leider meistens der Standard und alles weitere müssen wir spezifizieren.   
Der zweite Zeichensatz, den ich vorstellen will, ist *UTF-8*.    
Dieser beinhaltet eine ganze Menge mehr Zeichen und sogar Emojis.

```python
>>> print("Ärger")
Ärger
```

Python ist das format oftmals egal. Wir können einfach Sonderzeichen nutzen, wie wir wollen.    
Auch asiatische Characters sollten kein Problem sein.    
Viel mehr stoßen wir bei asiatischen Zeichen an die grenzen unserer Konsole:

```
E:\>py asia.py
𖡄𖡄𖡄𖡄𖡄
```

Wenn wir jedoch den Text in einen Text-Editor unserer Wahl kopieren erhalten wir die eigentlichen Zeichen.    
`こんにちは`

Also fassen wir noch einmal kurz zusammen. Es gibt unterschiedliche Zeichensätze.    
Python kann intern mit allen umgehen, aber unser Ausgabemedium vielleicht nicht.   

Kommen wir also zu dem eigentlich Interessanten an Strings; *Konvertierung*.   
Konvertieren wir für den anfang mal ein paar Zahlen:    

```python
>>> str(3)
'3'
>>> str(6)
'6'
>>> str(9999)
'9999'
```

Das ganze geht natürlich auch umgekehrt. Also vorausgesetzt wir haben es mit einer Zahl in Textformat zu tun.    
Ansonsten bekommen wir eine Fehlermeldung.

```python
>>> int('3')
3
>>> int('6')
6
>>> int("hallo")
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: invalid literal for int() with base 10: 'hallo'
```

Nun, was ist, wenn wir Strings aneinander heften wollen? Dafür einfach das Pluszeichen verwenden.

```python
>>> "Hi " + "Mom"
'Hi Mom'
```

Leider können wir das nur für Strings benutzen

```python
>>> "Ich bin " + 6 + " jahre alt"
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str
```

Reparieren wir also diese Nachricht:

```python
>>> "Ich bin " + str(6) + " jahre alt"
'Ich bin 6 jahre alt'
```

Diese Art und Weise Text zu schreiben und ihn mit Daten anzureichern ist zwar funktional, aber suboptimal.   
Wir wollen nämlich zwei weitere Eigenschaften von solchen Ausgaben.
- Sie sollen formatierbar sein
- Sie sollen im Code lesbar sein

Das ist leider bei dem oberen Ansatz nicht gegeben.   
Python hat eine zweite Art Strings. Die *formatted-Strings*.    
Diese kennzeichnen sich durch das `f` vor den `"`. (*f* wie *Format*)

```python
>>> f"Ich bin {6} jahre alt"
'Ich bin 6 jahre alt'
```

Der Inhalt in den `{}` wird automatisch in Text umgewandelt und formatiert.   
Hier ein paar nützliche formatierungsoptionen für Zahlen:

```python
>>> pi = 3.14159265359
>>> f"Pi ist {pi}"
'Pi ist 3.14159265359'
>>> f"Pi ist {pi:.2f}"
'Pi ist 3.14'
>>> f"{pi=}"
'pi=3.14159265359'
>>> yes_votes = 42572654
>>> no_votes = 43132495
>>> percentage = yes_votes / (yes_votes + no_votes)
>>> f'{yes_votes} YES votes -> {percentage:2.2%}'
'42572654 YES votes -> 49.67%'
```

Das letzte Beispiel ist besonders Hilfreich, wenn wir später Bugs in unseren Programmen suchen.    
Eine weitere Funktion, welche ich oft beim debuggen nutze, ist der `*` operator.

```python
>>> "_"*30
'______________________________'
```

Dieser Wiederholt die Zeichenkette (in unserem Beispiel) 30-mal.   
Damit lassen sich, zum Beispiel, sektionen in der Ausgabe gut voneinander trennen.

Zum Ende dieses Kapitels will ich noch auf direkte String-Funktionen eingehen.    
Besonders wichtig und oft benutzt sind `split`, `index`, `join` und `replace`.

```python
>>> "E:\\prog_projects\\10minutepython".split("\\")
['E:', 'prog_projects', '10minutepython']
```

Split teilt eine Zeichenkette in eine Liste auf.    
Damit haben wir den Speicherpfad aufgeteilt in seine Komponenten.   
Replace ist genauso einfach zu verstehen:

```python
>>> text = "Drei Chinesen mit dem Kontrabass"
>>> for vovel in "aeiou":
...   text = text.replace(vovel, "o")
...   print(text)
...
Drei Chinesen mit dem Kontroboss
Droi Chinoson mit dom Kontroboss
Droo Chonoson mot dom Kontroboss
Droo Chonoson mot dom Kontroboss
Droo Chonoson mot dom Kontroboss
```

Der erste Parameter ist die zu ersetzende Zeichenkette und der letzte Parameter ist das Zeichen, mit dem ersetzt wird.   

> **Aufgabe**:   
> Erweitere das *Drei Chinesen mit dem Kontrabass* Beispiel so, dass alle Versionen ausgegeben werden.    
> Beispielausgabe:   
> ```
> Draa Chanasan mat dam Kantrabass
> Dree Chenesen met dem Kentrebess
> Drii Chinisin mit dim Kintribiss
> Droo Chonoson mot dom Kontroboss
> Druu Chunusun mut dum Kuntrubuss
> ```

`index` gibt lediglich wieder, wo in der Zeichenkette wir einen bestimmten Wert finden können.

```python
>>> text = "blablablablablabloblablablabla"
>>> text.index("blo")
15
```

`join` ist wieder eine Funktion, welche am Anfang wahrscheinlich in der Ausgabe am besten benutzt werden kann:

```python
>>> text = ['Dies', 'ist', 'ein', 'kleiner', 'Grußtext']
>>> ", ".join(text)
'Dies, ist, ein, kleiner, Grußtext'
>>> " ".join(text)
'Dies ist ein kleiner Grußtext'
```

Wir machen aus einer iterierbaren Datenstruktur einen String mit ihren Teilnehmern 
verbunden durch die initiale Zeichenkette.

Für weitere Informationen über die Grunddatentypen kannst du immer die 
[offizielle Python Dokumentation](https://docs.python.org/3/library/stdtypes.html) befragen.

> **Aufgabe**:   
> Nun Testen wir das wissen, was in den letzten Kapiteln gelernt wurde.   
> Oftmals müssen wir Daten zwischen Programmen teilen oder zwischen Aufrufen speichern.   
> Speicher die folgende Liste als String in einer Variabel: `"[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"`.   
> Deine Aufgabe ist nun aus dem String wieder eine Liste mit Integers zu machen.

Auch wenn dies das Ende des Kapitels zu den Grunddatenstrukturen ist, ist das nicht das Ende von Datenstrukturen.   
Im nächsten Kapitel werden wir einen weiteren Weg lernen unsere Daten zu strukturieren.   
Der Grund weshalb ich gerade so viel Wert auf Datentypen lege, ist das Programmieren zu 70% daraus besteht,   
den richtigen Datentyp zu nutzen.    
Also, auch wenn es im Moment noch nicht in einem Gesamtbild zusammenkommen sollte,     
werden wir in unserem ersten Praxisprojekt schnell merken, wie wichtig das ist.    
Wir haben mit Abschluss dieses Kapitels tatsächlich schon alle Grundlagen für das erste Projekt gelegt.   
Deshalb werde ich in Kapitel 1.6 und 1.7 nicht neue Programmierelemente einführen, sondern Wege,    
unsere Programme zu strukturieren und organisieren.