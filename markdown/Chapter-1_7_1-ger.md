# Kapitel 1: Grundlagen

## Kapitel 1.7: Entwicklungspraktiken

### Kapitel 1.7.1: Virtual Environments

Wenn man sich einem größeren Projekt annimmt, so hat dieses meist andere Ziele als ein bisheriges Projekt.   
Ansonsten hätte man ja das bereits existierende nutzen können.   
Daraus folgt, dass viele Projekte unterschiedliche *Pakages* brauchen.   
Nun könnten wir alle Pakages natürlich in unserer Hauptinstallation von Python unterbringen.   
Diese würde dadurch jedoch sehr schnell, sehr groß werden und damit einhergehend auch sehr langsam.

Wie können wir also einzelnen Projekten erlauben ihre benötigten Packages zu installieren, aber ohne,
dass diese unsere Hauptinstallation beeinträchtigen?

Die Antwort auf diese Frage steht bereits im Titel des heutigen Kapitels: "**Virtual Environments**".
Virtual Environments, oder zu deutsch Virtuelle Umgebungen, sind separate, aber vollwertige Installationen von Python.

Python ermöglicht uns diese schnell und einfach in unseren Projekten zu generieren.

```
C:\Zwischenspeicher>py -m venv ./venv
```

Dieser Befehl erzeugt ein neues *Environment* in dem Ordner indem man sich momentan befindet.
Nach Konvention nennt man den Ordner indem das Virtual Environments erzeugt werden soll `venv`
(kruz für **V**irtual **Env**ironment).

Schauen wir uns den neuen Ordner etwas genauer an (die folgende Ausgabe ist leicht verkürzt):

```
C:\Zwischenspeicher>cd venv
C:\Zwischenspeicher\venv>tree
.
├───Include
├───Lib
│   └───site-packages
│       ├───pip
│       │   ├───_internal
│       │   │   ├───cli
│       │   │   ├───commands
│       │   │   ├───distributions
│       │   │   ├───...
│       │   ├───_vendor
│       │   │   ├───cachecontrol
│       │   │   │   ├───caches
│       │   │   ├───certifi
│       │   │   ├───chardet
│       │   │   │   ├───cli
│       │   │   │   ├───metadata
│       │   │   ├───...
│       ├───pip-22.3.dist-info
│       ├───pkg_resources
│       │   ├───extern
│       │   ├───_vendor
│       │   │   ├───importlib_resources
│       │   │   ├───jaraco
│       │   │   │   ├───text
│       │   │   ├───...
│       ├───setuptools
│       │   ├───command
│       │   ├───config
│       │   │   ├───_validate_pyproject
│       │   ├───extern
│       │   ├───...
│       ├───setuptools-65.5.0.dist-info
│       └───_distutils_hack
└───Scripts
```

Man kann anhand des Dateibaums erkennen, dass tatsächlich eine komplett neue Installation von Python vorliegt.   
Auch wenn dir viele der Ordnernamen noch nichts sagen werden, sind alle wichtig um Python so nutzen zu können,   
wie man es als Programmierer gewohnt ist.  
Aber wo befindet sich die `python.exe`?

**Aufgabe:**
> In der Ausgabe weiter oben sind nur Ordnernamen benutzt worden.  
> Erstelle dir nun ein eigenes `venv` und versuche in diesem die `python.exe` zu finden.   
> Falls du sie nicht finden kannst, kannst du einfach hier weiterlesen.

Die Antwort finden wir, wenn wir uns die Dateinamen anzeigen lassen:

```
...
└───Scripts
        ...
        python.exe
        ...
```

Aha, also befindet sich `Python.exe` immer unter `venv/Scripts/python.exe`.  
Mit diesem Wissen ausgestattet hält dich nun nichts mehr auf,
ein großes Projekt anzugehen und deine 1 Millionen Euro Applikation zu entwickeln.

Aber bevor du los stürmst um deine Ideen in die Tat umzusetzen,
wär es hilfreich noch zumindest Kapitel 1 zu Ende zu lesen.   
In den folgenden Kapiteln gehe ich, wie in diesem, noch auf weitere Praktiken ein,
welche das Programmieren in großen Systemen ermöglichen.