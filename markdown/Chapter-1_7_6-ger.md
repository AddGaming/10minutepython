# Kapitel 1: Grundlagen

## Kapitel 1.7: Entwicklungspraktiken

### Kapitel 1.7.6: Mark it down

In diesem letzten Teil der Einführung, will ich kein neues Programmierkonzept vorstellen,   
sondern stattdessen ein programmier-relevantes.

Oft kommt es vor, dass man Notizen machen muss.    
Natürlich kann man diese auf ein Blatt Papier notieren.    
Daran ist nichts auszusetzen und auch ich mache es regelmäßig, aber besonders, wenn es darum geht,     
diese Notizen für die Zukunft gut auffindbar und strukturiert abzuspeichern,    
kommt man mit analogen Papier auch eben nur so weit.

Stattdessen benutzen viele Programmierer eine **Mark-Up** Notierung um schnell und effizient Notizen machen zu können.

Die **Mark-Up-Sprache**, welche ich im folgenden Vorstelle heißt *"Mark-Down"*.    
Um ein *Mark-Down* File zu erstellen, erstellt man ein normales Text-File mit der Endung `.md`    
In diesem kann man nun mit minimaler Syntax Texteffekte einfügen.

Einige Beispiele:

| Beispiel           | Beschreibung                                                       |
|--------------------|--------------------------------------------------------------------|
| `**bold**`         | Damit kann man den Text innerhalb der `**` **dick** werden lassen. |
| `*italic*`         | Damit wird der Text innerhalb der `*` *Kursiv*.                    |
| `# Heading 1`      | Überschrift 1                                                      |
| `## Heading 2`     | Überschrift 2                                                      |
| \`code\` -> `code` | Sachen als Programmcode markieren mit `                            |

Auf dem [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
kann man die anderen Funktionalitäten nachschlagen.

Aber warum sollte man das in einem Mark-Down File machen und nicht in einem Word-Dokument?    
Die Antwort darauf ist, dass es viel schneller geht einfach mit nur minimaler Form seine Gedanken niederzuschreiben,   
anstatt mit den vielen Formatierungsoptionen von Word beschäftigt zu sein.

Ich persönlich mag sogar die minimale Ablenkung und die übersichtliche Text-Variante von Mark-Down so gerne,   
dass dieses "Buch" zu 100% in Markdown geschrieben wurde.   
Die einzelnen Dokumente können auf dem [Gitlab Projekt](https://gitlab.com/AddGaming/10minutepython) zu diesem Text gefunden werden.

**Aufgabe:**
> Schreibe ein kleines Mark-Down Dokument in einem Editor deiner Wahl.   
> Notiere in diesem, was du in den Letzten Tagen gelernt hast und was dir noch schwer fällt.   
> Mach eine weitere Liste mit Dingen, die du einmal programmieren willst.

_____

**Herzlichen Glückwunsch!**   
Du hast das erste Kapitel geschafft.   
Das ist eine bereits beachtliche Leistung! Sei stolz auf dich!    
Viele die mit Programmieren anfangen wollen, brechen nach weniger als 3 Tagen wieder ab.   
Der Fakt, dass du so lange weitergelesen hast, wie du es getan hast, ist etwas wofür du dich selbst loben solltest.

Ich hoffe ich konnte dir einen Einstieg in die Programmierung ermöglichen.   
Mit dem Wissen aus den vergangen Kapiteln solltest du in der Lage sein,
bereits sehr komplexe Programme zu schreiben.
Auch wenn es viel gibt, was ich nicht erklären konnte, gibt es nun nichts mehr, was dich stoppen sollte,
selbständig weitere Fortschritte zu erzielen.    
In Kapitel 2 stelle ich weitere Konzepte und Programme vor, welche alltagsnahe Anwendung finden können.
Falls keines der bereits erschienen Projekte in dem zweiten Teil interessieren,
empfehle ich dir stattdessen das dritte Kapitel zu lesen.    
In diesem Stelle ich weitere Resourcen vor, um mehr über Programmierung oder Python zu lernen.

Falls du Feedback bezüglich der nun hinter dir liegenden Kapitel haben solltest,
schreib es mir gerne auf allen Platformen, welche ich auf meinem [Profil](https://gitlab.com/AddGaming) verlinkt habe.