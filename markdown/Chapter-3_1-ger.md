# Kapitel 3: Wie gehts weiter?

## Kapitel 3.1: Youtube empfehlungen

Wenn du an diesem Kapitel angekommen bist, heißt dass, das dieses Buch zu ende ist.
An diesem Punkt hast du gelernt besser zu Programmieren, als viele Menschen, die Informatik studieren,
oder als Informatiker arbeiten.
Du solltest auch rausgefunden haben, ob dich das Thema interessiert und du vielleicht dich Beruflich in diese Richtung
orientieren willst.
Vielleicht willst du es aber nicht Beruflich machen, sondern als Hobby das ein oder andere Herzensprojekt umsetzen.
Im folgenden will ich dir Ressourcen ans Herz legen, welche mich als Programmierer weiter gebracht haben.

Der [Continuous Delivery](https://www.youtube.com/@ContinuousDelivery) Kanal von Dave Farley ist ein muss,
für jeden der in einem Team arbeiten will.
Auch wenn viele der Ideen für Leute, welche allein arbeiten wollen nur nette Tips und Tricks sind,
sind die Ratschläge aus seinen Videos für alle in einem Team arbeiten Programmierer unbeschreiblich wichtig.
Falls du also vor hast mit Freunden ein größeres Projekt anzugehen oder du in einem professionellen Kontext in einem
Team arbeiten willst, ist dieser Kanal eine Goldgrube.

Egal was deine Intentionen für die Zukunft sind, [Computerphile](https://www.youtube.com/@Computerphile)
ist wahrscheinlich ein Abo wert. Auf diesem Kanal werden regelmäßig Gäste eingeladen, welche verschiedenste
Themen der Informatik auf einfach Art und Weise erklären.
Egal ob Computersprachen bezogenes wie "Garbage Collection", Bildbearbeitung wie Kantenerkennung,
oder Hacking Themen wie Password-Cracking, auf diesem Kanal wirds du mit sicherheit etwas interessantes finden.

Wenn du mehr zu der Sprache Python wissen willst, kann ich dir [ArjanCodes](https://www.youtube.com/@ArjanCodes)
ans Herz legen.
Auf diesem Kanal dreht es sich hauptsächlich um Python und auch kompliziertere Themen, welche ich in diesem Buch
nicht behandelt habe, werden gut verständlich erklärt.

Falls dir Arjan immer noch nicht tief genug in die Materie geht und du wirklich auf einem Fundamentalen Level
verstehen willst, wie Python und andere Sprachen unter der Haube funktionieren,
kann ich dich [James Murphy](https://www.youtube.com/@mCoding) empfehlen.
In seinen Videos schaut er sich immer nur einen kleinen Teil der Sprache an.
Analysiert diesen jedoch ins kleinste Detail.

Wenn dich die Welt des Internets mehr interessiert und du gerne Mehr und komplexere Internetseiten bauen willst,
empfehle ich dir [Fireship](https://www.youtube.com/@mCoding).
Ursprünglich gegründet um zu erklären wie Googles Cloud-Storage anbieter mit verwandten Namen funktioniert,
beschäftigt sich der Kanal heute mit allem Tech und primär Internetbezogenen.
Eines der länger laufenden Formate in "X in 100 seconds" ermöglicht einen schnellen Einblick in unterschiedlichste
Technologien.

[LiveOverflow](https://www.youtube.com/@LiveOverflow)
[No Boilerplate](https://www.youtube.com/@NoBoilerplate)
[code_report](https://www.youtube.com/@code_report)
[DaFluffyPotato](https://www.youtube.com/@DaFluffyPotato)
[Freya Holmér](https://www.youtube.com/@Acegikmo)
[Retro Game Mechanics Explained ](https://www.youtube.com/@RGMechEx)
[Context Free](https://www.youtube.com/@contextfree)
[Engineer Man ](https://www.youtube.com/@EngineerMan)
[Clear Code ](https://www.youtube.com/@ClearCode)
[Dave's Space](https://www.youtube.com/@DavesSpace)
[Sebastian Lague ](https://www.youtube.com/@SebastianLague)
[Low Byte Productions](https://www.youtube.com/@LowByteProductions)
[Steve Brunton](https://www.youtube.com/@Eigensteve)
[SimonDev](https://www.youtube.com/@simondev758)
[PwnFunction ](https://www.youtube.com/@PwnFunction)
[Ben Eater ](https://www.youtube.com/@BenEater)
[NullPointer Exception](https://www.youtube.com/@NullPointerException)
[javidx9](https://www.youtube.com/@javidx9)

Natürlich unerlässlich für jeden anstrebenden Programmierer sind Konferenzen.
Auf diesen tauschen sich Experten und diese, welche einer werden wollen, über verschiedenste Dinge im Berufsalltag aus.
Deshalb empfehle ich hier einige Konferenzkanäle, welche viele ihrer Talks und Präsentationen hochladen:

- [GOTO Conferences](https://www.youtube.com/@GOTO-)
- [NDC Conferences](https://www.youtube.com/@NDC)
- [Game Developer Conference](https://www.youtube.com/@Gdconf)
- [Devoxx](https://www.youtube.com/@DevoxxForever)
- [DigiPen Game Engine Architecture Club](https://www.youtube.com/@GameEngineArchitects)
- [ccc.de](https://www.youtube.com/@mediacccde)
- [InfoQ](https://www.youtube.com/@infoq)
- [DevWeek Events](https://www.youtube.com/@devweekevents5952)
- [SIGGRAPH Advances in Real-Time Rendering](https://www.youtube.com/@siggraphadvancesinreal-tim4519)

Wenn du nicht weißt, mit welchen der Vorträgen du anfangen kannst oder solltest, starte einfach mit den populärsten
Vorträgen, oder den Titeln, welche dich ansprechen. Den Rest erledigt dann der Youtube Algorithmus ganz von alleine.