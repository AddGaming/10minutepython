# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

### Kapitel 2.1.7: Setting Settings

Das Letzte, was unserer `Settings` Klasse noch fehlt, ist eine Möglichkeit,
sie wären des Laufen des Programmes zu ändern.
Um das zu ermöglichen sind wir mit einer Herausforderung konfrontiert:
Wie können wir 1 Funktion schreiben, welche die Aufgabe für alle Parameter übernimmt?

Fangen wir erst einmal klein an.

```python
class Settings:
    ...

    def set(self, ___) -> ___:
        if not args:
            ...
            return

        if len(args) == 1:
            ...
        elif len(args) == 2:
            ...
        else:
            print(f"\tTo many arguments: {args}")

        self.save_to_file()
```

Definieren wir erst einmal für uns, was diese Funktion können soll.

- Wenn sie keine Argumente bekommt, soll sie die aktuellen Einstellungen ausgeben.
- Wenn sie 2 Argumente bekommt, soll sie den Wert des ersten Argumentes auf den des Zweiten setzen.
- Wenn sie 1 Argument bekommt, soll sie ausgeben, dass der Nutzer 2 Argumente braucht.
- Wenn sie mehr als 2 Argumente bekommt, soll sie dem Nutzer sagen, dass er sie falsch benutzt
- Alle Änderungen werden gespeichert

Ich haben bereits mit dem `else`-Fall den vorletzten letzten Punkt eingebaut.
Am Ende der Funktion speicher ich einfach immer den aktuellen Stand.
Dadurch werden auch immer automatisch alle Änderungen gespeichert.
Das heißt wir haben bereits 2 unserer 5 Punkte erledigt!

> - Wenn sie keine Argumente bekommt, soll sie die aktuellen Einstellungen ausgeben.
> - Wenn sie 2 Argumente bekommt, soll sie den Wert des ersten Argumentes auf den des Zweiten setzen.
> - Wenn sie 1 Argument bekommt, soll sie ausgeben, dass der Nutzer 2 Argumente braucht.
> - ✔️ Wenn sie mehr als 2 Argumente bekommt, soll sie dem Nutzer sagen, dass er sie falsch benutzt
> - ✔️ Alle Änderungen werden gespeichert

Arbeiten wir nun am ersten Punkt.
Wir haben genau das in den letzten Kapiteln mehrfach gemacht.
Weißt du, was gemacht werden muss? Versuche den Teil selbst aufzuschreiben und kontrolliere danach deinen Ansatz,
mit dem meinen:

```python
class Settings:
    ...

    def set(self, *args) -> ___:
        if not args:
            for key, val in self.__dict__.items():
                print(f"\t{key}: {val}")
            return

        if len(args) == 1:
            ...
        elif len(args) == 2:
            ...
        else:
            print(f"\tTo many arguments: {args}")

        self.save_to_file()
```

Damit können wir auch den ersten Punkt auf unserer Liste abhaken:

> - ✔️ Wenn sie keine Argumente bekommt, soll sie die aktuellen Einstellungen ausgeben.
> - Wenn sie 2 Argumente bekommt, soll sie den Wert des ersten Argumentes auf den des Zweiten setzen.
> - Wenn sie 1 Argument bekommt, soll sie ausgeben, dass der Nutzer 2 Argumente braucht.
> - ✔️ Wenn sie mehr als 2 Argumente bekommt, soll sie dem Nutzer sagen, dass er sie falsch benutzt
> - ✔️ Alle Änderungen werden gespeichert

Machen wir weiter mit Punkt 2.
Wenn wir 2 Argumente bekommen, setzen wir diese Argumente.
Um das machen zu können, müssten wir entweder eine Menge `if`s schreiben:

```python
class Settings:
    ...

    def set(self, *args) -> ___:
        if not args:
            for key, val in self.__dict__.items():
                print(f"\t{key}: {val}")
            return

        if len(args) == 1:
            ...
        elif len(args) == 2:
            if args[0] == "default_source":
                ...
            elif args[0] == "default_destination":
                ...
            if args[0] == "file_format_list":
                ...
        else:
            print(f"\tTo many arguments: {args}")

        self.save_to_file()
```

oder wir benutzen die Funktion `setattr()`.
`setattr()` nimmt dabei 3 Argumente; das Objekt, den Namen des Attributes und den neuen Wert.

```python
class Settings:
    ...

    def set(self, *args) -> ___:
        if not args:
            for key, val in self.__dict__.items():
                print(f"\t{key}: {val}")
            return

        if len(args) == 1:
            ...
        elif len(args) == 2:
            setattr(self, args[0], args[1])
        else:
            print(f"\tTo many arguments: {args}")

        self.save_to_file()
```

Ah, dass sieht doch schon viel ordentlicher aus.
Aber halt! Da könnte der Nutzer doch sonnst noch was eingeben!
Das muss natürlich kontrolliert werden.
Fangen wir mit der offensichtlichen Kontrolle an.
Nutzer sollen keine neuen Attribute definieren können.
Um das zu verhindern benutzen wir die Funktion `getattr()`, welche uns entweder den Wert des Attributes zurück gibt,
oder uns einen Fehler gibt, dass dieses Attribut nicht existiert.
Und wie wir mit Fehler umgehen, wissen wir bereits:

```python
class Settings:
    ...

    def set(self, *args) -> ___:
        if not args:
            for key, val in self.__dict__.items():
                print(f"\t{key}: {val}")
            return

        if len(args) == 1:
            ...
        elif len(args) == 2:
            try:
                getattr(self, args[0])
                setattr(self, args[0], args[1])
            except AttributeError:
                print(f"\tAttribute '{args[0]}' doesn't exists")
        else:
            print(f"\tTo many arguments: {args}")

        self.save_to_file()
```

Nun haben wir aber auch den Sonderfall,
dass sowohl `default_source` und `default_destination` valide Ordner sein müssen.
Also bauen wir auch hier eine Ausnahmeregel ein, die kontrolliert,
dass beiden nur valide Adressen zugeordnet werden können.

```python
class Settings:
    ...

    def set(self, *args) -> ___:
        if not args:
            for key, val in self.__dict__.items():
                print(f"\t{key}: {val}")
            return

        if len(args) == 1:
            ...
        elif len(args) == 2:
            if args[0] == "default_source" or args[0] == "default_destination":
                if not os.path.isdir(args[1]):
                    print(f"\tThe path you entered is not valid.\n\tpath: {args[1]}")
                    return
            try:
                getattr(self, args[0])
                setattr(self, args[0], args[1])
            except AttributeError:
                print(f"\tAttribute '{args[0]}' doesn't exists")
        else:
            print(f"\tTo many arguments: {args}")

        self.save_to_file()
```

> - ✔️ Wenn sie keine Argumente bekommt, soll sie die aktuellen Einstellungen ausgeben.
> - ✔️ Wenn sie 2 Argumente bekommt, soll sie den Wert des ersten Argumentes auf den des Zweiten setzen.
> - Wenn sie 1 Argument bekommt, soll sie ausgeben, dass der Nutzer 2 Argumente braucht.
> - ✔️ Wenn sie mehr als 2 Argumente bekommt, soll sie dem Nutzer sagen, dass er sie falsch benutzt
> - ✔️ Alle Änderungen werden gespeichert

Puh, fasst fertig.
Programmieren wir noch schnell die entsprechenden Fehlermeldungen bei dem Fall, dass ein Argument benutzt wird:

```python
class Settings:
    ...

    def set(self, *args) -> None:
        """
        Sets the param_name to the value of param.
        If no args are passed on, it instead returns the current settings
        :param param_name: str - the name of the attribute you want to edit
        :param value: str - the value you want it to set to
        :return: None
        """
        if not args:
            for key, val in self.__dict__.items():
                print(f"\t{key}: {val}")
            return

        if len(args) == 1:
            try:
                print(f"\t{args[0]}: {getattr(self, args[0])}")
                print("If you want to change the Parameter, use 'settings <parameter> <new_value>'")
            except AttributeError:
                print(f"\tAttribute '{args[0]}' doesn't exists")
        elif len(args) == 2:
            if args[0] == "default_source" or args[0] == "default_destination":
                if not os.path.isdir(args[1]):
                    print(f"\tThe path you entered is not valid.\n\tpath: {args[1]}")
                    return
            try:
                getattr(self, args[0])
                setattr(self, args[0], args[1])
            except AttributeError:
                print(f"\tAttribute '{args[0]}' doesn't exists")
        else:
            print(f"\tTo many arguments: {args}")

        self.save_to_file()
```

Damit ist die Settings Klasse fertig und wir können sie im Code nutzen.
Fangen wir also nun schnell an, die Klasse in unseren Code zu benutzen.
Als erstes aktivieren wir die `set()` Methode in unserer `__init__.py`

```python
"""
Contains the functions for the CLI.
This file contains the mapping and administration of these functions
"""

from .cli_functions import copy_from, print_tree
from .settings import Settings

SETTINGS = Settings()


def give_help() -> None:
    ...


# register command for cli
registered = {
    "import": copy_from,
    "tree": print_tree,
    "help": give_help,
    "quit": quit,
    "exit": quit,
    "settings": SETTINGS.set,
}
```

Als nächstes gehen wir in unsere `copy_from()` Funktion und unsere `print_tree()` Funktion,
und stellen diese so ein, dass wenn sie keine Argumente bekommen, sie die Standardargumente aus unserem
`SETTINGS` Objekt nutzen:

```python
"""
The functions for the CLI
"""
import os
import shutil

from . import SETTINGS


def copy_from(source: str = "", destination: str = "", endings: [str] = None) -> None:
    if not source:
        source = SETTINGS.default_source
    if not destination:
        destination = SETTINGS.default_destination
    if not endings:
        endings = SETTINGS.file_format_list

    ...


def print_tree(path: str = "", depth: int = 1) -> None:
    if not path:
        path = SETTINGS.default_source

    ...
```

Probieren wir doch nun das ganze einmal aus:

```
$: settings
    default_source: E:\prog_projects\10minutepython\Project1\CLI
    file_format_list: ['.png', '.jpg']
    default_destination: E:\prog_projects\10minutepython\Project1\CLI
$: settings default_source 
    default_source: E:\prog_projects\10minutepython\Project1\CLI
If you want to change the Parameter, use 'settings <parameter> <new_value>'
$: settings default_source E:\\prog_projects\\10minutepython\\Project1
$: settings
    default_source: E:\\prog_projects\\10minutepython\\Project1
    file_format_list: ['.png', '.jpg']
    default_destination: E:\prog_projects\10minutepython\Project1\CLI
$: tree
|--CLI
|--|--cli_functions.py
|--|--settings.json
|--|--settings.py
|--|--__init__.py
|--|--__pycache__
|--|--|--cli_functions.cpython-310.pyc
|--|--|--settings.cpython-310.pyc
|--|--|--__init__.cpython-310.pyc
|--main.py
|--__pycache__
$: quit

Process finished with exit code 0
```

Ich verspreche dir, dass auch das Kopieren der Dateien fehlerfrei funktioniert, auch wenn ich das in diesem
Ausführungsprotokoll nicht demonstriert habe.
Damit ist auch dieses Kapitel fast zu ende.
Ich hoffe dir hat die Reise zu einer ersten Vollwertigen App spaß gemacht und dir gezeigt,
dass man sich viel Zeitaufwand sparen kann, wenn amn ein paar Zeilen Python schreibt.
Unser Programm umfasst 252 Zeilen und hat 10 Kapitel, also 100 Minuten gedauert zu schreiben.
Dabei hast du in fasst jedem Kapitel eine neue Facette von Python kennen gelernt.
Das Endresultat ist dabei auch noch Fehlertolerant und kann (auf keinen mir bekannten Weg) zum absturz gebracht werden.
Du hast jedes Recht, stolz auf dich zu sein!

Im nächsten Kapitel gehe ich auf einen Aspekt an, den ich bisher vernachlässigt habe: Testen.