# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

### Kapitel 2.1.3: Rekursion

Nachdem wir beim letzten Mal den Grundstein dafür gelegt haben,
dass wir arbeiten können, schauen wir uns heute ein Konzept an, mit dem wir unserem Ziel näher kommen können.

Die Rede ist hierbei von *Rekursion*.

Bevor wir also am "echten" Programm weiter schreiben, hier eine simplifizierte darstellung von Rekursion:

Angenommen wir wollen zwei Zahlen multiplizieren und angenommen es gäbe den "`*`" nicht.
Wie würden wir die Multiplikation selbst implementieren?

Mathematisch lässt sich `5*5` zu `5+5+5+5+5` übersetzen.
Das heißt, solange wir addieren können, können wir die Aufgabe mit wiederholter Addition lösen.
Nun stoßen wir jedoch schnell an unsere momentanen Grenzen, wenn wir das programmieren wollen.

```python
def mul(a,b):
    return a+a+a+a
```

Das würde zwar funktionieren, aber nur solange wie b = 5.
Der aufmerksame Leser wird schon bereits an `for-loops` gedacht haben.
Und tatsächlich können wir damit das Problem lösen.

```python
def mul(a,b):
    result = a
    for _ in range(b-1):
        result += a
    return result
```

Aber das löst unser problem auch nur so zur hälfte:

```python
>>> def mul(a,b):
...     result = a
...     for _ in range(b-1):
...         result += a
...     return result
...
>>> mul(3,3)
9
>>> mul(0,2)
0
>>> mul(2,0)
2
```

Das ist nicht das, was wir erwarten würden.
Aber wir können die Funktion auch anders schreiben.

```python
def mul(a,b):
    if a == 0 or b == 0:
        return 0
    return a + mul(a, b-1)
```

Diese Implementierung Funktioniert. Rekursion heißt, dass sich die Funktion selbst aufruft.

```python
>>> def mul(a,b):
...     if a == 0 or b == 0:
...         return 0
...     return a + mul(a, b-1)
...
>>> mul(3,3)
9
>>> mul(0,3)
0
>>> mul(3,0)
0
>>> mul(3,1)
3
```

Aber wie genau funktioniert sie jetzt?
Gehen wir hierfür das Beispiel `mul(3,3)` durch.

Wir gehen in die Funktion und sehen als erstes das `if-statement`.
Dieses wertet zu falsch aus, da sowohl `a = 3 != 0` und `b = 3 != 0`
Also gehen wir zum `return-statement` und geben `a` also `3` zurück.
Aber halt. Da steht ein `+` hinten dran.
Also werten wir jetzt noch die rechte Seite der Addition aus.
Unsere Antwort ist also `a + (mul(a, b-1))` oder `3 + (mul(3, 2))`.
Im nächsten Durchlauf erhalten wir `a + (a + (mul(a, b-1)))` oder konkret `3 + (3 + (mul(3, 1)))`.
Danach `a + (a + (a + mul(a, b-1)))` oder konkret `3 + (3 + (3 + mul(3, 0)))`.
Und somit aktiviert sich unsere `if`-Klausel in dem vierten Aufruf der Funktion endlich.
Unsere Antwort ist also: `3 + (3 + (3 + 0))`.
Lassen wir die Klammern fallen weil es sich um eine Addition handelt, sehen wir, dass unser Programm uns wirklich
durch wiederholtes Selbstaufrufen `3 + 3 + 3 + 0` = `3 + 3 + 3` = `3 * 3` ausrechnet.

Hier noch ein paar weitere Ausführungsprotokolle für andere Zahlen.

##### `mul(2, 1)`

- `2 + mul(2, 0)`
- `2 + 0`
- `2`

##### `mul(4, 5)`

- `4 + mul(4, 4)`
- `4 + 4 + mul(4, 3)`
- `4 + 4 + 4 + mul(4, 2)`
- `4 + 4 + 4 + 4 + mul(4, 1)`
- `4 + 4 + 4 + 4 + 4 + mul(4, 0)`
- `4 + 4 + 4 + 4 + 4 + 0`
- `20`

Aber warum brauchen wir das so "kompliziert"?
Warum können wir nicht mit `for` und `if` uns Händisch durch die Ausnahmen und Sonderfälle durcharbeiten?

```python
def mul(a,b):
    if a == 0 or b == 0:
        return 0
    result = a
    for _ in range(b-1):
        result += a
    return result
```

Das mag zwar für einen so einfachen Fall wie diesen noch Funktionieren, aber was,
wenn wir verschiedene Sachen für unterschiedliche Eingaben machen wollen?
Was, wenn wir nicht nur Zahlen, sondern auch Strings und Arrays akzeptieren oder mehr ifs und elses
in das Programm einbauen?
Das ganze wird ohne Rekursion recht schnell unübersichtlich.
Genug aber der Theorie.
Wie sieht das ganze jetzt für unseren konkreten Fall aus?

Damit wir über Dateistrukturen gehen können, brauchen wir zwei Funktionen aus `os`.
Die erste ist `os.listdir(path)`, welche uns eine Liste der Elemente in einem Ordner (eng. Directory) gibt
und `os.path.isdir(path)`, welche uns sagt, ob ein Pfad ein Ordner ist, oder nicht.
Also schreiben wir uns mit diesen beiden Funktionen mal eine Funktion, welche durch eine Ordnerstruktur durchgeht.
Wenn du willst, kannst du es zuerst alleine probieren, bevor du dir die Lösung anschaust.

```python
def print_tree(path: str, depth: int = 1) -> None:
    """
    prints the structure of a given folder | if path leads to file, only file path is printed
    :param path: str - the path
    :param depth: int - current recursion depth
    :return: None
    """
    if os.path.isdir(path):
        for element in os.listdir(path):
            print(depth * "|--" + element)
            element_path = f"{path}{os.sep}{element}"
            if os.path.isdir(element_path):
                print_tree(element_path, depth=depth + 1)
    else:
        print(path)
```

Ich habe den Parameter `depth` hinzugefügt, damit die Ausgabe etwas lesbarer wird.
Um zu demonstrieren, wie genau die Funktion nun auf einem Ordner arbeitet,
schauen wir uns doch mal den Ordner an, in dem ich gerade dieses Buch schreibe:

```
>>> print_tree("E:\\prog_projects\\10minutepython")
|--.git
|--|--...
|--.gitlab-ci.yml
|--.idea
|--|--...
|--convert.py
|--html
|--|--chapter_nav.html
|--|--greeting.html
|--|--template.html
|--markdown
|--|--Chapter-1_1-ger.md
|--|--Chapter-1_2-ger.md
|--|--Chapter-1_3_1-ger.md
|--|--...
|--Project1
|--|--main.py
|--|--__pycache__
|--public
|--|--Chapter-1_1-ger.html
|--|--Chapter-1_2-ger.html
|--|--Chapter-1_3_1-ger.html
|--|--...
|--|--chapters.css
|--|--codehilite.css
|--|--cover.css
|--|--generic.css
|--|--index.html
|--README.md
|--requirements.txt
>>>
```

Aus Platzgründen habe ich die Ausgabe mit `...` etwas verkürzt, aber die Funktion scheint zu Funktionieren.
Probiere sie ruhig auf einigen deiner Ordner aus.
Aber fürs erste sollten wir sie in unser Programm integrieren.
Das ist auch das letzte, was wir Heute machen.
Nächstes mal schauen wir uns an, wie wir jetzt damit Dateien importieren können.

**main.py**

```python
"""
Starting Point of the CLI
Programm to import and de-duplicate files from an external device
"""

def print_tree(path: str, depth: int = 1) -> None:
    """
    prints the structure of a given folder | if path leads to file, only file path is printed
    :param path: str - the path
    :param depth: int - current recursion depth
    :return: None
    """
    if os.path.isdir(path):
        for element in os.listdir(path):
            print(depth * "|--" + element)
            element_path = f"{path}{os.sep}{element}"
            if os.path.isdir(element_path):
                print_tree(element_path, depth=depth + 1)
    else:
        print(path)

if __name__ == "__main__":
    os.chdir(os.sep.join(__file__.split(os.sep)[:-1]))
    
    while True:
        cmd_param = input("$: ")
        if cmd_param.split()[0] == "quit":
            quit()
        if cmd_param.split()[0] == "tree":
            print_tree(cmd_param.split()[1])
        else:
            print(f"unrecognized input: {cmd_param.split()[0]}")
```