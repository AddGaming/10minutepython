# Kapitel 1: Grundlagen

## Kapitel 1.7: Entwicklungspraktiken

### Kapitel 1.7.2: Testing

Testing von Programmcode ist wichtig und sollte die Grundlage jeder Softwareentwicklung sein.
Viele Anfänger denken sich: "Ich sehe doch ob es funktioniert. Warum soll ich Zeit mit Tests verschwenden?"

Diese Denkweise ist auf unsäglich vielen Ebenen falsch.
So vielen, dass tatsächlich viele schlaue und lange Bücher geschrieben wurden,
mit dem einzigen Ziel diese Denkweise zu wiederlegen.
Deshalb spare ich mir an dieser Stelle auch darauf weiter einzugehen.
Der geneigte Leser wird schon die Ressourcen finden.

Statt dessen will ich etwas Aufmerksamkeit darauf lenken, wie das Testen von Software eine art Stützrad sein kann,
welches einem Programmieren und Aufmerksamkeit beibringt.

Angenommen wir haben eine angemessen komplexe Funktion vor uns.
In meinem Beispiel: Das Kombinieren von 2 Dictionaries.

Das Standardverhalten in python ist das Folgende:

```python
def add_dictionaries(d1: dict, d2: dict):
    d1.update(d2)
    return d1


dic1 = {"a": 1, "b": 2, "c": 3}
dic2 = {"c": 4, "d": 5}
print(add_dictionaries(dic1, dic2))

{'a': 1, 'b': 2, 'c': 4, 'd': 5}
```

Es fällt auf, dass bei schlüssel `c` der Wert aus `dic2` genommen wurde und der aus `dic1` verschwunden ist.
In der Funktion, welche ich aber brauche, sind die alten Werte immer noch enthalten.

Aber anstatt direkt die neue Funktion zu schreiben, schreiben wir nun zuerst die Tests.
Die Tests werden eine ausführbare Spezifikation für die Funktion bilden und mit versichern, dass alles so funktioniert,
wie ich es wollte und meine Funktion keine Bugs hat.

Das die aktuelle Version der Funktion bereits eine Bugquelle ist, fällt auf,
wenn ich mir mehr Informationen ausgeben lasse.

```python
dic1 = {"a": 1, "b": 2, "c": 3}
dic2 = {"c": 4, "d": 5}
print(add_dictionaries(dic1, dic2))
print(f"{dic1=}")
print(f"{dic2=}")

{'a': 1, 'b': 2, 'c': 4, 'd': 5}
dic1 = {'a': 1, 'b': 2, 'c': 4, 'd': 5}
dic2 = {'c': 4, 'd': 5}
```

Oha! Da hat sich einfach still und heimlich der wert von dic1 geändert.
Sowas bezeichnet man als "Seiteneffekt" und wenn man ein paar von diesen an Orten hat, wo man sie nicht Vermutet,
wird man viel Spaß bei der Fehlersuche haben.

Schreiben wir also nun als ersten Test, einen, der genau dieses Szenario abfängt:

```python
import unittest


class AddDictionaries(unittest.TestCase):

    def test_function_does_not_affect_input(self):
        dic1 = {"a": 1, "b": 2, "c": 3}
        dic2 = {"c": 4, "d": 5}
        add_dictionaries(dic1, dic2)
        self.assertEqual(dic1, {"a": 1, "b": 2, "c": 3})
        self.assertEqual(dic2, {"c": 4, "d": 5})
```

Hier sind nun ein paar Sachen auf einmal Passiert:

1. Ich habe eine Testklasse erstellt mit dem Namen `AddDictionaries`
2. Ich habe einen Testfall mit hilfe einer neuen Funktion definiert (`test_function_does_not_affect_input`)
3. Dieser Funktion habe ich einen Namen gegeben, welcher mit später genau sagt, was hier getestet werden soll
4. In der Funktion habe ich die Funktion einmal mit dem selben Eingaben wie im Beispiel aufgerufen.
5. Im anschluss teste ich mit einem `assert` (de. Zusichern), dass alles richtig ist.

Dabei testet `assertEqual` auf Gleichheit.
Ich kann also davon ausgehen, dass dieser Test fehlschlagen wird.
Führen wir ihn einmal aus, indem wir folgendes unten an den Testdatei anhängen:

```python
if __name__ == "__main__":
    unittest.main()
```

**Ausgabe:**

```
======================================================================
FAIL: test_function_does_not_affect_input (__main__.AddDictionaries.test_function_does_not_affect_input)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "c:\Zwischenspeicher\git_demo\dict_fusion.py", line 13, in test_function_does_not_affect_input
    self.assertEqual(dic1, {"a": 1, "b": 2, "c": 3})
AssertionError: {'a': 1, 'b': 2, 'c': 4, 'd': 5} != {'a': 1, 'b': 2, 'c': 3}
- {'a': 1, 'b': 2, 'c': 4, 'd': 5}
?                       ^^^^^^^^^

+ {'a': 1, 'b': 2, 'c': 3}
?                       ^


----------------------------------------------------------------------
Ran 1 test in 0.001s

FAILED (failures=1)
```

Es schlägt fehl! Hurra!
Schauen wir uns die Fehlermeldung etwas genauer an.
In der ersten Zeile finden wir, welche Funktion fehlgeschlagen ist (`test_function_does_not_affect_input`).
Den Namen haben wir absichtlich so gewählt, dass wir genau wissen, warum er fehlgeschlagen ist;
Die Funktion beeinflusst die Eingaben.
Darunter finden wir den `Traceback`. Also eine Aufschlüsselung, wo unser Programm fehlgeschlagen ist.

```
AssertionError: {'a': 1, 'b': 2, 'c': 4, 'd': 5} != {'a': 1, 'b': 2, 'c': 3}
- {'a': 1, 'b': 2, 'c': 4, 'd': 5}
?                       ^^^^^^^^^
```

Hier weist uns Python darauf hin, dass `{'a': 1, 'b': 2, 'c': 4, 'd': 5}`
nicht unserem erwarteten `{'a': 1, 'b': 2, 'c': 3}` entspricht und markiert uns,
an welchen Stellen sie beiden sich unterscheiden.

Ich könnte nun also auf 100 Zeilen versuchen Verbal zu beschreiben, wie ich will, das die Funktion funktioniert,
oder ich zeige dir die Test, die ich dazu geschrieben habe:

```python
class AddDictionaries(unittest.TestCase):

    def test_function_does_not_affect_input(self):
        # Testet ob die Funktion die Eingabeparameter nicht beeinflusst
        ...

    def test_overlapping_keys_result_in_tuple(self):
        # Testet ob übereinstimmende Schlüssel ein Tupel mit allen Inhalten an dieser Stelle bilden
        ...

    def test_not_overlapping_keys_stay_original_values(self):
        # Testet ob keine übereinstimmenden Schlüssel den selben Wert wie in der Eingabe erhalten
        ...

    def test_fusing_with_empty_dict_does_not_do_anything(self):
        # Testet, dass das fusionieren von einem Gefüllten und einem Lehren in dem Gefüllten ended 
        ...
```

Falls du der englischen Sprache mächtig bist, verstehst du nun perfekt,
wie die Funktion `add_dictionaries` funktioniert.
Und das beste, den Beweis, dass sie tatsächlich funktioniert, bekommst du gleich mitgeliefert.

Implementieren wir also schnell die Tests.

**Aufgabe:**
> Versuche ruhig selbst die Tests zu implementieren.
> Du hast bereits das nötige wissen.
> Falls du anhand der Funktionsnamen nicht herleiten kannst, was getestet werden soll, lies einfach weiter

**Lösung:**

```python
class AddDictionaries(unittest.TestCase):

    def test_function_does_not_affect_input(self):
        dic1 = {"a": 1, "b": 2, "c": 3}
        dic2 = {"c": 4, "d": 5}
        add_dictionaries(dic1, dic2)
        self.assertEqual(dic1, {"a": 1, "b": 2, "c": 3})
        self.assertEqual(dic2, {"c": 4, "d": 5})

    def test_overlapping_keys_result_in_tuple(self):
        dic1 = {"a": 1, "b": 2, "c": 3}
        dic2 = {"c": 4, "d": 5}
        answer = add_dictionaries(dic1, dic2)
        self.assertEqual((3, 4), answer["c"])

    def test_not_overlapping_keys_stay_original_values(self):
        dic1 = {"a": 1, "b": 2, "c": 3}
        dic2 = {"c": 4, "d": 5}
        answer = add_dictionaries(dic1, dic2)
        self.assertEqual(1, answer["a"])

    def test_fusing_with_empty_dict_does_not_do_anything(self):
        dic1 = {}
        dic2 = {"c": 4, "d": 5}
        answer = add_dictionaries(dic1, dic2)
        self.assertEqual(dic2, answer)
```

Ich hoffe es ist nun klar, wie sehr einem Tests helfen können.
Man findet viel schneller Probleme im Code, wenn man ihn schon zerstören will, wenn er noch nicht einmal existiert.
Aber nur so erhält man Code, der genau das tut, was man von ihm will.
Und wenn man immer damit anfängt, Tests zu schreiben, kommt man auch nie in die Position,
in welcher der Code hat zu testen ist.

**Aufgabe:**
> Von der einen Aufgabe in die nächste.
> Schaffst du, die Funktion `add_dictionaries` so zu implementieren, dass sie alle Tests besteht?
> Schreib die Funktion ruhig in die selbe Datei.
> Unter dem Aufgabentext findest du die gesamte Datei

**dict_fusion.py**

```python
import unittest


def add_dictionaries(d1: dict, d2: dict):
    # Schreibe die Funktion
    ...


class AddDictionaries(unittest.TestCase):

    def test_function_does_not_affect_input(self):
        dic1 = {"a": 1, "b": 2, "c": 3}
        dic2 = {"c": 4, "d": 5}
        add_dictionaries(dic1, dic2)
        self.assertEqual(dic1, {"a": 1, "b": 2, "c": 3})
        self.assertEqual(dic2, {"c": 4, "d": 5})

    def test_overlapping_keys_result_in_tuple(self):
        dic1 = {"a": 1, "b": 2, "c": 3}
        dic2 = {"c": 4, "d": 5}
        answer = add_dictionaries(dic1, dic2)
        self.assertEqual((3, 4), answer["c"])

    def test_not_overlapping_keys_stay_original_values(self):
        dic1 = {"a": 1, "b": 2, "c": 3}
        dic2 = {"c": 4, "d": 5}
        answer = add_dictionaries(dic1, dic2)
        self.assertEqual(1, answer["a"])

    def test_fusing_with_empty_dict_does_not_do_anything(self):
        dic1 = {}
        dic2 = {"c": 4, "d": 5}
        answer = add_dictionaries(dic1, dic2)
        self.assertEqual(dic2, answer)


if __name__ == "__main__":
    unittest.main()
```