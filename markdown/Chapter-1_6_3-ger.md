# Kapitel 1: Grundlagen

## Kapitel 1.6: Files

### Kapitel 1.6.3: Packaging

Betrachten wir den Code von gestern.    
In diesem haben wir Code, basierend auf seiner Funktionalität in verschiedene Files gepackt.   
Nun ist es glaub ich dennoch ersichtlich, dass dadurch Files entstehen,
welche zwar inhaltlich nicht identisch sind (weshalb wir überhaupt eine Aufteilung durchgeführt haben)   
aber dennoch thematisch zusammen gehören.

Im Beispiel von gestern gehören die Funktionen `prozent` und `hoch2` thematisch zusammen.   
Normalerweise gruppieren wir Dateien, welche thematisch zusammen gehören, intuitiv zusammen.

Du wirst vielleicht einen Ordner "Bilder" auf deinem Computer haben.   
In diesem sind vielleicht Unterordner wie "Sommerurlaub-2019" oder "Ausflug-Schwarzwald".  
Genauso macht man es auch mit Code.

Also Packen wir mal die zwei Dateien von gestern in einen Ordner:

**Ordnerstruktur:**

```
main.py
mathe
  |--funktionen.py
  |--prozent.py
```

Damit haben wir jedoch wieder unser Programm kaput gemacht.  
Damit wir Code so schachteln können, müssen wir aus dem Ordner ein *Package* machen.  
Das geht zum glück sehr einfach.
Wir müssen dem Ordner nur eine Datei mit dem Namen `__init__.py` hinzufügen.

```
main.py
mathe
  |--__init__.py
  |--funktionen.py
  |--prozent.py
```

Die Datei (`__init__.py`) darf ruhig leer sein.
Passen wir jetzt noch die Imports in `main.py` an.

```python
from mathe.funktionen import hoch2
from mathe.prozent import prozent
```

Aber warum sollten wir uns diesen Mehraufwand machen?  
Ist es nicht ausreichend einfach den Code gut zu separieren und dann die Dateien zu importieren, wenn man sie brauch?

Die Antwort auf diese Frage ist leider etwas komplizierter, als ich zugeben möchte.  
Rein *theoretisch* ist der oben beschriebene Ansatz nicht *"falsch"*.  
Er läd nur sehr leicht dazu ein, schlechten, unwartbaren Code zu schreiben.  
Am Ende des Tages sind wir alle nur Menschen und wir sollten Code so strukturieren,
dass wir ihn einfach benutzen können.

Der Vorteil an dieser Paketstruktur ist, dass wenn wir ein neues Projekt haben,
und Funktionalität aus einen anderen wiederverwenden wollen,   
wir einfach das entsprechende Paket rüberkopieren können.

Wenn die Dateien jedoch lose rumfliegen, dann wird es oft so sein,
dass man eine zur Ausführung benötigte Datei (*eng. Requirement*) dabei vergisst.

Somit führen Packages zu einer angenehmeren Erfahrung beim wiederverwenden von Code.

Weitere Argumentationen für das Nutzen von Packages werden sich in Kapitel 2 des Buches finden,
wenn ich kompliziertere Programme herleiten werde.

Also beenden wir das Kapitel 1.6 mit einer letzten Übung.

**Aufgabe:**
> Erweitere die Anwendung um ein neues Package "text".  
> In diesem Package definiere eine neue Funktion `greet` (*eng. für grüßen*).  
> Diese Funktion grüßt alle Namen, welche sie übergeben bekommt.  
> Wenn du dich nicht an alle String-Operationen erinnern kannst,
> befrag ruhig noch einmal das Internet oder Kapitel `Kapitel 1.4.2: Text`   
> Beispiel:
>> $: greet Peter Hans Markus    
> > Hi Peter    
> > Hi Hans   
> > Hi Markus
> 