# Kapitel 1: Grundlagen

## Kapitel 1.6: Files

### Kapitel 1.6.2: Mehrere Files und imports

Im letzten Kapitel habe ich vorgestellt, wie man seine Programme in Dateien speichern kann.   
Nun kommt es des öfteren vor, dass man nicht nur ein kleines Skript zur einmaligen Nutzung schreibt,   
sondern ein größeres Programm basierend auf mehreren *Files*.

Angenommen man schreibt eine kleine Applikation, die man im Nachhinein erweitern will.

```python
while True:
    inpt = input("$: ")
```

Der kleine Programmschnipsel frägt jetzt den Nutzer permanent nach einer neuen Eingabe.   
Das beenden dieses Programms geht *nur* über `STRG + C` was ein sehr uneleganter Weg ist, ein Programm zu beenden.   
Passen wir nun das Programm so an, dass wir es einfacher beenden können:

```python
while True:
    inpt = input("$: ")
    if inpt == "quit":
        quit()
```

Besser. Nun können wir mit der Eingabe "quit" (*eng. für verlassen/beenden*) das Programm wieder schließen.

Integrieren wir nun Funktionalität in die Anwendung.   
Ich werde im Folgenden mathematische Operationen nutzen,
da ich in diesem Kapitel nicht mehr eigenheiten von Python vorstellen möchte,
sondern mich stattdessen auf das Verwenden mehrerer Dateien fokussieren möchte.

Schreiben wir nun also eine Funktion "`hoch2()`" welche ein Zahl quadriert.

```python
def hoch2(zahl):
    return zahl * zahl


while True:
    inpt = input("$: ")
    if inpt == "quit":
        quit()
    if inpt == "hoch2":
        antwort = hoch2()
        print(antwort)
```

Falls es nicht bereits aufgefallen sein sollte; diese Implementation funktioniert auf diese Weise **nicht**!

**Aufgabe:**
> Finde mindestens 1 Grund, warum diese Implementation kaput geht.   
> (Es gibt insgesamt 3)

**Bonus Aufgabe:**
> Kannst du das Programm reparieren?   
> Versuche es ruhig!
> Falls du nicht mehr weiter kommst, kannst du einfach hier weiter lesen.

Zuerst der offensichtlichste Fehler:   
Die Funktion `hoch2` erwartet einen Parameter, bekommt aber nie einen.

```python
if inpt.startswith("hoch2"):
    antwort = hoch2(inpt)
    print(antwort)
```

Das wird leider auch nicht ganz funktionieren. Wir erwarten schließlich eine Zahl, und keinen Text.

```python
if inpt.startswith("hoch2"):
    antwort = hoch2(inpt.replace("hoch2", ""))  # ersetzt "hoch2" durch nichts -> ""
    print(antwort)
```

Das ist schon besser.   
Nun wird nur noch die Zahl an `hoch2` weitergegeben.   
Aber so richtig wird dies immer noch nicht funktionieren.   
`hoch2` würde zwar nun die richtige Eingabe erhalten, jedoch nicht richtig mit ihr rechnen,
da die Eingabe immer noch ein Text ist, und keine Zahl.

```python
def hoch2(zahl):
    zahl = int(zahl)
    return zahl * zahl
```

So, jetzt funktioniert das Ganze auch:

```
$: hoch2 2
4  
```

Nun mag das gut klappen, solange nur 1 Funktion existiert, aber was, wenn wir 10, 20, oder 100 Funktionen haben?   
Die Datei würde unweigerlich zu groß und unübersichtlich werden.   
Daher teilt man die Funktion des Programms in mehrere Dateien auf.   
Als Faustregel kann man sagen, dass jede Datei, nur ein Thema behandeln sollte.

Folgen wir diesem Ratschlag in unserem Beispiel, so sollten wir die Funktion `hoch2` auslagern.   
Der Startpunkt eines Programmes wird konventionell "main" genannt.
Also teilen wir die Funktionalität auf:

**main.py**

```python
while True:
    inpt = input("$: ")
    if inpt == "quit":
        quit()
    if inpt.startswith("hoch2"):
        antwort = hoch2(inpt.replace("hoch2", ""))
        print(antwort)
```

**funktionen.py**

```python
def hoch2(zahl):
    zahl = int(zahl)
    return zahl * zahl
```

Soweit so gut, aber wenn wir nun `main.py` ausführen, erhalten wir einen Fehler:

```
$: hoch2 2
Traceback (most recent call last):
  File "c:\Zwischenspeicher\cli.py", line 6, in <module>
    antwort = hoch2(inpt.replace("hoch2", ""))
              ^^^^^
NameError: name 'hoch2' is not defined
```

Damit wir die Funktion aus einer anderen Datei nutzen können, müssen wir diese importieren.

```python
from funktionen import hoch2
```

Diese Zeile ist auch wieder Wörtlich zu lesen: *Von* **funktionen** *importiere* **hoch2**.

Bevor es zur heutigen Aufgabe geht, hier die vollständigen Dateien:

**main.py**

```python
from funktionen import hoch2

while True:
    inpt = input("$: ")
    if inpt == "quit":
        quit()
    if inpt.startswith("hoch2"):
        antwort = hoch2(inpt.replace("hoch2", ""))
        print(antwort)
```

**funktionen.py**

```python
def hoch2(zahl):
    zahl = int(zahl)
    return zahl * zahl
```

**Aufgabe:**
> Definiere eine neue Funktion `prozent`, welche als Argument eine Zahl nimmt und diese durch 100 teilt.   
> Definiere diese Funktion in einer neuen Datei namens `prozent.py` und importiere sie in das Hauptprogramm.   
> Beispiel:
>>
>> $: prozent 5   
> > 0.05   
> > $: prozent 100   
> > 1.0   
