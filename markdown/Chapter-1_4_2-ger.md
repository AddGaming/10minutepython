# Kapitel 1: Grundlagen

## Kapitel 1.4: Typen und Operatoren

### Kapitel 1.4.2: Zahlen

Heute schauen wir uns einen weiteren Typ an, der weit verbreitet ist; Zahlen.

In Programmiersprachen unterscheidet man Zahlen grob in 2 Kategorien.      
Die ganzen Zahlen (*Integer* oder *int*) und Komma-Zahlen (*Floating point numbers* oder *float*).     
Integer haben wir bereits in vielen der hier vorgestellten Programme genutzt.     
Floats verhalten sich sehr ähnlich, weshalb ich zuerst auf die Eigenheit von Komma-Zahlen in Computern eingehen
will,    
bevor ich die Funktionsweise von Zahlen erkläre.

Computer haben keinen Weg nativ auf Komma-Zahlen zu rechnen, oder sie überhaupt zu speichern.     
Also, wie kommt es, dass Python und viele andere Programmiersprachen auf Floats rechnen können?    
Sie bedienen sich eines Tricks. Auch wenn die genaue Funktionsweise des Tricks hier grad zu viel wäre,    
hat das wiederrum Konsequenzen für uns als Entwickler.    
Betrachten wir die folgenden Brüche:

```python
>> > 1 / 3
0.3333333333333333
>> > 5 / 7
0.7142857142857143
>> > 3 / 7
0.42857142857142855
```

Nah? Ist dir das Problem aufgefallen?    
Auf den ersten Blick nicht, weil es aussieht wie die Antwort auf deinem Taschenrechner, aber genau das ist das
Problem!   
Taschenrechner und alle anderen Computer müssen Runden.    
Das führt zu Mathematischen ungenauigkeiten, wenn mit den Zahlen weitergerechnet werden soll.   
Als Faustregel kannst du dir merken, dass Kommazahlen problemlos genutzt werden können,     
solange du *nicht mehr als 6 Stellen präzession* brauchst.

Hier eine Liste mit den häufigsten Operationen auf Zahlen:

| Operation           | Beispiel                                                 | Beschreibung                                                                                          |
|---------------------|----------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| Ganzzahlumwandlung  | `int(5.0)` -> `5`                                        | Konvertiert die Eingabe in einen Integer                                                              |
| Kommazahlumwandlung | `int(5)` -> `5.0`                                        | Konvertiert die Eingabe in einen Float                                                                |
| Vergleiche          | `5>2` -> `True`<br>`2<=2` -> `True`<br> `5==5` -> `True` | Vergleicht die beiden Zahlenwerte und gibt den entsprechenden Bool zurück.                            |
| Maximum             | `max(2,3)` -> `3`                                        | Nimmt beliebig viele Parameter oder eine iterierbare Datenstruktur und gibt den maximalen Wert zurück |
| Minimum             | `min(2,3)` -> `2`                                        | Nimmt beliebig viele Parameter oder eine iterierbare Datenstruktur und gibt den minimalen Wert zurück |

Dazu kommen noch die bereits gelernten Operatoren für Addieren, Subtrahieren, Multiplizieren und Dividieren.   
Falls das immer noch nicht ausreichen sollte, um deine Rechnung durchzuführen, kannst du dich bei der `math` Bibliothek bedienen.   
Auf die Funktionalität dieser gehe ich nicht ein, da das für den Moment den Ramen sprängt und ihr,    
falls euch das interest, die [Dokumentation](https://docs.python.org/3/library/math.html) durchlesen könnt.

> **Aufgabe**:   
> Erstelle eine Funktion `rel_to_abs(percent, base)` welche als Parameter eine Prozentzahl und eine Grundgröße nimmt.   
> Die Funktion soll die absolute Größe in Relation zu der Grundgröße ausrechnen. Beispiele:   
> `rel_to_abs(20,100)` -> `20`  (lese: 20% von 100 -> 20)   
> `rel_to_abs(200,50)` -> `100` (lese: 200% von 50 -> 100)  
> `rel_to_abs(0.1,1000)` -> `1` (lese: 0.1% von 1000 -> 1)

Ich möchte aber dem folgenden Abschnitt einen einzelnen Operator näher beleuchten, der vielen Einsteigern ungeläufig sein sollte.   
`%` ist der Modulo-Operator. Dieser gibt uns den Rest in einer Ganzzahldivision zurück.    
`4 / 3` hat beispielsweise den Rest `1`. Daher gibt uns `4 % 3` als Antwort `1`.    

Aber warum ist diese Restsache für uns so wichtig, wenn wir Computerprogramme schreiben?    
Das sieht man schnell an einem Beispiel:

```python
def ist_gerade(zahl):
  if (zahl % 2) == 0:
    return True
  else:
    return False
```

Wenn der Rest in der Division gleich 0 ist, haben wir es mit einer geraden Zahl zu tun.    
Dies ist auch nützlich, wenn wir auf iterierbaren Datenstrukturen arbeiten.   
Diese haben inherent ein Attribut *Länge* (`len()`) und wenn wir Elemente anfordern,    
welche nicht in der Liste sind, bekommen wir Fehler.    
Das ist nur logisch, wenn man darüber nachdenkt.   
Wenn ich eine Liste mit 5 Elementen habe, und ich frage nach dem 6ten, kann ich das 6te Element nicht erhalten,   
weil es nicht auf der Liste steht.

```python
>>> mein_tuple = (0,1,2,3)
>>> mein_tuple[2]
2
>>> mein_tuple[5]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: tuple index out of range
```

Solche Fehler können wir auch mit dem `%` und der Funktion `len()` vermeiden.

```python
>>> mein_tuple[2 % len(mein_tuple)]
2
>>> mein_tuple[5 % len(mein_tuple)]
1
```

Diese Technik habe ich beispielsweise bei Zufallszahlengenerierung in einem meiner Spiele genutzt.    
Auch wenn in der Funktion Dinge zum Tragen kommen, welche wir noch nicht gelernt haben,    
will ich doch zumindest einmal ein Beispiel aus der "echten Welt" zeigen:

```python
def buff_random(self, atk: int, hp: int, permanent: bool = False):
    """
    Buffs a random Pokémon in this team. Negative values de-buff.
    :param atk: int - the attack increase amount
    :param hp: int - the hit-points increase amount
    :param permanent: if the buff should be permanent
    :return: None
    """
    if permanent:
        self.__pokemons[int(self) % self.__size].buff_permanent(hp, atk)
    else:
        self.__pokemons[int(self) % self.__size].buff_temp(hp, atk)
```

Achte auf den Inhalt in den `[]`. Der funktioniert genauso wie das andere Beispiel:    
`mein_tuple[2         % len(mein_tuple)]`   
`__pokemons[int(self) % self.__size]`

Auch wenn `%` anfangs vielleicht gewöhnungsbedürftig ist, ist es ein sehr starkes Werkzeug.

> **Aufgabe**:   
> Schreibe eine Funktion `advnace_time(current_hours, advancy_by)` welche als Parameter    
> die aktuelle Stunde und die anzahl Stunden bekommt, um welche die Zeit voranschreiten soll.    
> Sie gibt die neue Uhrzeit in digitaler Notation zurück.       
> Beispiele:   
> `advnace_time( 3, 40)` -> ` 7`   
> `advnace_time( 1, 15)` -> `16`   
> `advnace_time(23,  2)` -> ` 1`   

> **Aufgabe**:   
> Schreibe eine Funktion `digital_to_analog(hours, minutes)` welche als Parameter    
> die aktuellen Stunden und Minuten bekommt und ein Tupel der Form `(stunde, minute)` in analoger Zeitnotation zurückgibt.   
> Beispiele:
> `digital_to_analog( 3, 40)` -> `( 3,40)`
> `digital_to_analog(15, 40)` -> `( 3,40)`
> `digital_to_analog(23, 59)` -> `(11,59)`