# Kapitel 1: Grundlagen

## Kapitel 1.7: Entwicklungspraktiken

### Kapitel 1.7.3: GIT (is) good

Ich hoffe, dass ich mit GIT eine Software im Folgenden vorstelle,
welche auch außerhalb von Softwareentwicklung Anwendung finden kann.
Git ist ein Versionskontrollprogramm.
Oder anders ausgedrückt: Ein Weg nie wieder eine Datei zu verlieren.

Das ist eine große Behauptung, aber wie genau soll das gehen?

Nachdem ihr [Git installiert](https://git-scm.com/downloads) habt,
könnt ihr einfach von der Consol aus in dem entsprechenden Verzeichnis `git init` ausführen.

Dies initialisiert Git und erschafft einen `.git` Ordner.
Innerhalb dieses Ordners legt Git nun all deine Daten mit sehr hoher, aber Verlust freier, Komprimierung eure Daten ab.

Um deine Daten nun dem Archiv hinzuzufügen, müssen sie dem zuerst beobachtet (eng. tracked) werden.
Dies geht mit dem Befehl `git add [Dateiname]` oder wenn man einfach Alles tracken möchte `git add .`.

Jetzt wo Git auf die Dateien aufpasst,
können wir einen Schnappschuss des aktuellen Zustandes mit `git commit -m"nachricht"` speichern.
Dabei ersetzt man den Text innerhalb der " mit einer kurzen Beschreibung der Änderungen.
Dies dient dazu, dass man im nachhinein noch nachvollziehen kann,
was für eine Version sich hinter der Versionsnummer versteckt.

Alles zusammengenommen sieht der erste Befehlsfluss bei der Initialisierung von Git folgender Maßen aus:

```
C:\Zwischenspeicher\git_demo>git init
Initialized empty Git repository in C:/Zwischenspeicher/git_demo/.git/

C:\Zwischenspeicher\git_demo>git add .

C:\Zwischenspeicher\git_demo>git commit -m"initial commit"
[master (root-commit) 94d4ac3] initial commit
 1 file changed, 1 insertion(+)
 create mode 100644 readme.md
```

Um die Versionierungsfähigkeiten von Git demonstrieren zu können, erstelle ich nun eine neue Version:

```
C:\Zwischenspeicher\git_demo>echo "diese lies-mich hat nun inhalt" > readme.md

C:\Zwischenspeicher\git_demo>echo "Datei nummer 2" > file2.txt

C:\Zwischenspeicher\git_demo>git add .

C:\Zwischenspeicher\git_demo>git commit -m"created file2 | updated readme"
[master 5ac026c] created file2 | updated readme
 2 files changed, 2 insertions(+), 1 deletion(-)
 create mode 100644 file2.txt
```

Also, was genau habe ich hier gemacht:
Zuerst habe ich den Text der in `readme.md` zu finden ist geändert.
Danach habe ich eine zweite Datei erstellt und sie `file2.txt` genannt.
Als letztes habe ich Git ge-updated mit einem neuen Schnappschuss des Ordners.

Aber das war alles nur der Aufbau um vorführen zu können, das mit Git keine Datei mehr verloren geht!

Mit `git reflog` können man sich anschauen, wie der Versionsverlauf aussieht:

```
C:\Zwischenspeicher\git_demo>git reflog
5ac026c (HEAD -> master) HEAD@{0}: commit: created file2 | updated readme
94d4ac3 HEAD@{1}: commit (initial): initial commit
```

Man kann erkennen, dass wir 2 Versionen registriert haben.
Version `5ac026c` und `94d4ac3`.
Rechts neben den Versionsnummern kann man die Nachrichten lesen,
mit welchen ich die einzelnen Schnappschüsse abgespeichert hatte.

Machen wir also eine Zeitreise:

```
C:\Zwischenspeicher\git_demo>dir
...
23.12.2022  22:15    <DIR>          .
23.12.2022  22:15    <DIR>          ..
23.12.2022  22:15                19 file2.txt
23.12.2022  22:15                35 readme.md
               2 Datei(en),             54 Bytes

C:\Zwischenspeicher\git_demo>git checkout 94d4ac3
Note: switching to '94d4ac3'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by switching back to a branch.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -c with the switch command. Example:

  git switch -c <new-branch-name>

Or undo this operation with:

  git switch -

Turn off this advice by setting config variable advice.detachedHead to false

HEAD is now at 94d4ac3 initial commit
```

Der erste Befehl, den ich hier ausgeführt habe, war um noch einmal darzustellen, wie der Ordner momentan aussieht.
Man kann erkennen, das der Ordner 2 Dateien hat: `file2.txt` und `readme.md`.
Mit dem Befehl `git checkout [versionsnummer]` bin ich auf die Versionsnummer gewechselt.
Die lange Nachricht von Git heißt vereinfacht, dass wir in einem *Anschauen aber nicht Anfassen* Modus sind.

Kontrollieren wir nun also, ob die Zeitreise tatsächlich funktioniert hat:

```
C:\Zwischenspeicher\git_demo>dir
...
23.12.2022  22:22    <DIR>          .
23.12.2022  22:22    <DIR>          ..
23.12.2022  22:22                16 readme.md
               1 Datei(en),             16 Bytes

C:\Zwischenspeicher\git_demo>type readme.md
"readme text"
```

Tatsächlich. Es ist nur noch 1 Datei im Ordner und auch der Inhalt dieser Datei ist der Selbe,
wie er zum Zeitpunkt des Schnappschusses war.

Gehen wir nun also zurück in die Zukunft Morty.

```
C:\Zwischenspeicher\git_demo>git switch master
Previous HEAD position was 94d4ac3 initial commit
Switched to branch 'master'

C:\Zwischenspeicher\git_demo>dir
...
23.12.2022  22:28    <DIR>          .
23.12.2022  22:28    <DIR>          ..
23.12.2022  22:28                19 file2.txt
23.12.2022  22:28                35 readme.md
               2 Datei(en),             54 Bytes

C:\Zwischenspeicher\git_demo>type readme.md
"diese lies-mich hat nun inhalt"

C:\Zwischenspeicher\git_demo>
```

Mit `git switch master` habe ich wieder auf Master zurückgewechselt.
Die anderen Befehle sollten bereits bekannt sein und validieren, dass wir wieder in der Gegenwart angekommen sind.

Die Superkraft die Git einem also verleiht sind also unglaublich nützlich.
Brauchst du eine Datei nicht mehr? - Lösche sie ruhig. Git erinnert sich für dich.
Hast du etwas geändert und auf einmal funktioniert nichts mehr? - Gehe eine Version zurück und alles ist wieder gut.

Git hat mir bereits Projekte gerettet, bei denen ich ohne Git Monate an Arbeit verloren hätte.
Daher kann ich nur empfehlen, egal worum es geht, nutze Git
und mach es dir zu einer Angewohnheit regelmäßig einen Schnappschuss abzuspeichern mit einer sinnvollen Nachricht
an dein zukünftiges Ich.

**Aufgabe:**
> Tracke mit Git ein Verzeichnis, welches Dateien beinhaltet, welche du nicht verlieren möchtest.