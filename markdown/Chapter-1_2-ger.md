# Kapitel 1: Grundlagen

## Kapitel 1.2: Was sind Funktionen und Variablen?

Vielleicht ist dieser Titel etwas verwirrend für nicht Mathematiker.   
Daher hier eine kurze Erklärung, was in der Informatik und Mathematik unter Funktion verstanden wird:   

>Eine Funktion ist keine Funktionalität, sondern eine Abstraktion/Generalisierung um bestimmtes verhalten universell anwenden zu können.   

Falls dieser Satz Kopfschmerzen verursacht, ist das kein Problem.   
Es lässt sich besser an einem Beispiel verstehen.   

Ich muss oft, wenn ich mal rechnen muss, Sachen mehrfach ausrechen.   
In der Schule sollte jeder bereits einmal Kontakt mit Variabeln in der Mathematik gemacht haben.   
Diese sind tatsächlich der erste Schritt in die richtige Richtung.   

Angenommen ich rechne aus, ob und wie neues Mobiliar in mein Zimmer passt.   
Ich könnte also immer und immer wieder die Maße meines Zimmers in den Taschenrechner einhämmern    
oder ich lege sie in einer Variable an.   

In Python würde das ganze so aussehen:

```python
>>> laenge = 4
>>> breite = 4
>>> hoehe = 2
```

Ihr merkt, dass wir nicht auf 1 Zeichen beschränkt sind.    
Das war bereits in der Schule der Fall, aber die Lehrer sagen es meistens nicht.   
Die Zeiten von `y = (x^2)*z` und anderen unverständlichen Abkürzungen sind also vorbei!   
Wenn ich mir nun anschauen will, welcher Wert gerade in einer Variabel gehalten wird,    
kann ich einfach ihren Namen in der Konsole eingeben.   

```python
>>> hoehe
2
```

>**Aufgabe:**   
>Lege ein paar Variabeln in deiner Consol an.   
>Das geht indem du dich an folgendes Schema hälst: `[name] = [wert]`   
>Ähnlich also, wie ich es etwas weiter Oben vorgemacht habe.   

Aber warum sollte ich das machen?   
Generalisiert könnte man sagen, dass es den Code besser lesbar macht.   
Meistens rechnen wir nicht einfach schnell was aus, wie im Taschenrechner,    
sondern schreiben Programme, welche wir am Tag danach, oder ein Jahr später benutzen wollen.    
Wenn wir dann von ein haufen Zahlen ohne Kontext begrüßt werden,    
dauert es lange herauszufinden, was dieses Programm überhaupt macht.   

Der eigentliche Nutzen hinter Variablen wird aber erst klar, wenn wir uns Funktionen anschauen.   
Wenn ich nun die m^2 meines Zimmers ausrechen will, kann ich das natürlich per Hand machen:   

```python
>>> laenge * breite
16
```

Aber ich will das nicht immer alles per Hand neu eingeben.   
Deshalb definiere ich mir eine neue Funktion, die dies für mich übernimmt.   

```python
>>> def flaeche():
...   return laenge * breite
...
>>> flaeche()
16
```

Nun kann ich durch das aufrufen einer Funktion immer und zuverlässig die Fläche meines Raumes ausgeben.   
Eine Funktion definieren wir also folgendermaßen:   

```
def [name]():
  return [wert]
```

Hierbei ist `name` der Name der Funktion und `wert` der Rückgabe wert.   
Wichtig ist auch, im *Funktionskörper* alles einzurücken.    
Der etablierte Standard für die Consol ist hierbei zwei Leerzeichen.    
An diesem Punkt fragt sich der aufmerksame Leser vielleicht, wofür die Klammern hinter dem Namen gut sind.   
Gestern haben wir diese nämlich gefüllt   

```python
print("Hi Mom")
```

Die Antwort ist einfach. Wir können in den Klammern Parameter mitgeben, auf denen die Funktion danach arbeitet.   
Betrachten wir nochmal die Funktion `flaeche()`.   
Diese gibt zwar die Fläche des aktuellen Raumes wieder, aber leider auch nur die Fläche dieses einen Raumes.   
Wenn wir mit der Funktion die Fläche aller Räume der WELT ausrechen wollen, müssen wir sie *parametrisieren*   

```python
>>> def flaeche(laenge, breite):
...   return laenge * breite
```

Wir haben nun also aus unseren zwei Werten, die in der Funktion verrechnet werden,   
Werte gemacht, die wir übergeben müssen.   
Rufen wir die Funktion nochmal auf, wie gerade eben   

```python
>>> def flaeche(laenge, breite):
...   return laenge * breite
...
>>> flaeche()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: flaeche() missing 2 required positional arguments: 'laenge' and 'breite'
```

Wie Python uns mitteilt, MÜSSEN wir diese Parameter nun IMMER mitgeben, wenn wir diese Funktion aufrufen.   
Wie man Fehlermeldungen liest ist jedoch Thema für ein anderes Mal.   
Rufen wir die Funktion also richtig auf:   

```python
>>> flaeche(laenge, breite)
16
```

Und das ganze funktioniert nun für jedes Zahlenpaar:   

```python
>>> flaeche(1,2)
2
>>> flaeche(20,4)
80
```

Dieses Konzept ist sehr stark und wird der metaphorische Ziegelstein aus dem wir unsere Programme bauen werden.   
Auch wenn ich erst Morgen mit dem nächsten Konzept näher darauf eingehen kann, wie man diese halbwegs altagsnah nutzt,   
hoffe ich, dass etwas klar wurde, wieso Funktionen hilfreich sind.   

>**Als letzte Aufgabe für Heute:**   
>Schreibe eine Funktion `volumen` welche als Parameter die Länge, Breite, und Höhe akzeptiert.   
>Lösungen dürfen gerne in diesen Thread gepostet werden, MÜSSEN aber mit dem `/spoiler` am Anfang versehen werden!   
