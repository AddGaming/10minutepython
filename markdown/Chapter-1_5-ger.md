# Kapitel 1: Grundlagen

## Kapitel 1.5: Klassen und Objekte

In Kapitel 1.2 habe ich grob die Funktionsweise eines Funktionsobjektes erklärt.   
In mathematischer Notation:   
`f(x) = x*2`   
oder in Python:   
`def f(x): return x*2`   

In diesem Beispiel haben wir bereits eine andere Funktion genutzt um "f" zu definieren.   

**Aufgabe:**    
>Kannst du dir denke welche?   

Halte diesen Gedanken im Hinterkopf. Zuerst müssen wir uns nämlich um Klassen kümmern.   

```python
class MyClass:
  pass
```

Wir definieren Klassen mit dem Wort "class" und danach den Namen.   
Das Wort "pass" ist ein Platzhalter der Python signalisiert,    
dass er einfach mal nichts machen soll.   
Eine Klasse an sich ist Nutzlos. Sie dient lediglich als Bauplan.   
Nutzen wir also den Bauplan und bauen ein Objekt:   

`first_object = MyClass()`

Hopla, wieso rufen wir eine Klasse wie eine Funktion auf?   
Die Antwort ist einfach; wir rufen eine Funktion auf.   
Wo die herkommt und wie die aussieht, schauen wir uns ein anderes Mal an.   
Aber was ist das Ergebnis der Funktion?   
Genau, ein Objekt dass nach dem Bauplan unserer Klasse gebaut wurde.   

Aber momentan ist unser Bauplan etwas leer.   
Fügen wir also ein paar Informationen hinzu:   

```python
class MyClass:
  x = 5
  y = "Peter"
```

Was du jetzt siehst ist, dass Variablennamen wichtig sind.   
Auch wenn wir Klassen nutzen, um Verhalten im echten Leben zu modellieren,   
bringt dies nichts, wenn wir am nächsten Tag nicht erkennen können,    
was wir damit gemeint hatten.   

```python
class Person:
  age = 5
  name = "Peter"
```

Ahh, schon besser.

**Aufgabe:**    
>Baue eine weitere Klasse in deiner Konsole und gib ihr Atribute.   
>Achte dabei auf die Einrückung. (2 Leerzeichen Einrückung ist ein Standart)   

Nun haben wir endlich unsere eigenen Objekte,    
aber wie können wir uns die in der Konsole ausgeben lassen?   
Betrachten wir folges Szenario:   

```python
>>> peter = Person()
>>> peter
<__main__.Person object at 0x0000017D6B61F400>
>>>
```

Unsere Konsole gibt uns zwar die Information,    
dass es sich um ein Objekt der Klasse Person handelt,    
und ebenfalls der Ort, wo sich unser Objekt im Speicher des Computer befindet,    
aber das ist... nicht sehr hilfreich in den Meisten fällen.   

Die Konventionen Pythons legen uns nahe,    
dass wir im folgenden unser Objekt auf folgende Art und Weise eine entsprechende Textrepräsentation geben:   

```python
class Person:
  age = 5
  name = "Peter"
  def __str__(self):
    return "Ich bin " + self.name + " und bin " + str(self.age) 
```

Whoa, whoa, was ist das "`__str__`" denn für ein komischer Name für eine Funktion?   
Python hat, um die Lesbarkeit zu erhöhen, einige Funktionen vorgeschrieben.    
Das ermöglicht, dass man sie einfacher nutzen kann.   

In diesem Fall ist "`__str__`" der verlangte Name für die Funktion "`str()`".   
`str()` konvertiert die Eingabe in eine Textrepresentation.   
Ich benutze in dem Beispiel `str()` um das Alter von einer Zahl,    
zu einem Text zu machen mit `str(self.age)`.   
Es gibt aber noch einen anderen weg die Funktion zu nutzen:   

```python
peter.__str__()
```

ist das Selbe wie    

```python
str(peter)
```

Aber ich glaube du merkst, dass das eine etwas einfacher zu lesen ist.   
Aber genug gelabert. Wie sieht das alles jetzt in der Konsole aus?   

```python
>>> str(peter)
'Ich bin Peter und bin 5'
>>>
```

**Aufgabe:**    
>Definiere eine str() methode für deine Klasse und teste sie in der Konsole.   
>Vergiss nicht, wenn du die definition der Klasse geändert hast auch ein neues Objekt zu erzeugen.   



Und hier noch die Lösung für die aller erste Frage:   

>**Refresher:**
>```
>def f(x): return x*2
>```
>In diesem Beispiel haben wir bereits eine andere Funktion genutzt um "f" zu definieren.   
>Kannst du dir denke welche?   

**Antwort:**   
Wir haben die Funktion `__mul__()` der Zahlen-Klasse genutzt.    
Glücklicherweise ist die bereits vorimplementiert.   
Angenommen `a = 3` und `b = 4`:   

`a * b` 

ist hierbei das Selbe wie

`a.__mul__(b)`

Und in der Konsole kommt das Selbe raus:

```python
>>> a*b
12
>>> a.__mul__(b)
12
>>>
```

Spätestens hier sollte der Vorteil der vordefinierten Namen eindeutig werden.   

**Bonus Aufgabe:**   
>Definiere in deiner Klasse eine Multiplikationsfunktion.    
>Da das wahrscheinlich für die Klasse, die du dir ausgedacht hast, keinen Sinn macht,   
>ist diese Aufgabe zusatz.   
