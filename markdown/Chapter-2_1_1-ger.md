# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

### Kapitel 2.1.1: Main Loop

Man kann Anwendungen (eng. Applications oder Apps) grob in 2 Kategorien einteilen.
Kommandozeilenanwendungen (CLIs) oder graphische Apps (GUIs).
In diesem Kapitel werden wir den Grundstein einer CLI anfertigen.
Dieser mag komplizierter erscheinen als nötig ist, jedoch sorgt dieser etwas umständliche Anfang dafür,
dass die selbe Struktur, sogar der selbe Code, in zukünftigen Projekten benutzt werden kann.
Also tauschen wir etwas Einfachheit mit Wiederverwertbarkeit.

Legen wir uns zuerst einen neuen Ordner an und legen uns ein Venv innerhalb dieses an.
Wenn du damit fertig bist, sollte die Ordnerstruktur etwa so aussehen:

```
Project1
  |--main.py
  |--venv
      |--...
      ...
```

Innerhalb der main.py fangen wir jetzt an zu coden.
In meiner IDE habe ich konfiguriert, dass jedes neues Python-File automatisch mit folgenden Inhalt generiert wird:

```python
"""
TODO:
    file documentation
"""

if __name__ == "__main__":
    ...
```

Hier sehen wir direkt 2 neue dinge.
Das erste ist, dass die Datei mit einem Text (String) anfängt.
Ich meine wirklich den Datentyp aus Python, nicht den Inhalt dessen.
Ein solcher Text, welcher nicht einer Variabel zugewiesen ist und direkt am Anfang steht,
wir auch DocString (kurz für Documentation String) genannt.
Die Aufgabe dieses Textblockes ist es, nicht im Programm Verwendung zu finden, sondern dem Programmierer mitzuteilen,
was nicht durch den Code ersichtlich sein sollte.

Erledigen wir also direkt das erste TODO und schreiben in diesen Docstring, was wir von diesem Programm
und dieser Datei erwarten:

```python
"""
Starting Point of the CLI
Programm to import and de-duplicate files from an external device
"""

if __name__ == "__main__":
    ...
```

Also nun zum zweiten Teil, welcher neu sein sollte.
`if __name__ == "__main__"`
Aus dem kapitel zu Klassen, wissen wir, dass interne operationen von Python oft die Characteristic aufweisen,
diese doppelten Unterstriche im Namen zu haben.
Das kennen wir bereits von `__init__.py` in Paketen.
Mit `__name__` erhalten wir den Namen der Python Datei.
Sollte diese Datei die erste sein, welche aufgerufen wurde, vergibt Python ihr einen neuen Namen; `__main__`.
Also, sollten wir `main.py` von irgendwo importieren, wird die Datei den Namen `main` haben und nicht `__main__`.

An dieser Stelle rate ich zum selbständigen Experimentieren.
Ich werde in diesen Kapiteln keine Aufgaben mehr an den Leser stellen und hoffe,
dass die natürliche Neugierde ausreicht, um sich Fragen selbst zu stellen und zu klären.

Da dieses Konzept jedoch neu ist, hier eine kleine Hilfestellung zu Experimenten, welche man selbst unternehmen sollte:

```python
print(__name__)

print("Nicht im Block")

if __name__ == "__main__":
    print("im block")
```

Erstelle dir eine Datei, welche grob so aussieht, wie den Code, welchen ich oben angeführt habe.
Importiere diese Datei und lass von ihr importieren.
Füge Code an stellen ein und schau ob dieser ausgeführt wird, oder ausgeführt werden kann.

Das nötige Verständnis, welches für dieses Kapitel jedoch nur von belang ist, ist dass dieser Block dafür sorgt,
dass wir den Code darin nur ausführen, wenn diese Datei der Einstiegspunkt der Ausführung ist.

Schauen wir uns nun noch eine zweite Variabel an, welche uns Python bereit stellt.
`__file__` gibt uns den vollständigen Pfad, an welchem sich die gerade ausgeführte Datei befindet.
Auch hier rate ich zu kleinen Experimenten, um zu verstehen, welchen Wert diese variabel hat.
Ich erwähne die Variable hier, aber sie wird leider erst im nächsten Teil Anwendung finden.

Beenden wir also die Arbeit an dem Programm für Heute damit, dass wir unseren Haupt-Loop (eng. Main-Loop) aufsetzen.

```python
if __name__ == "__main__":

    while True:
        cmd_param = input("$: ")
        if cmd_param.split()[0] == "quit":
            quit()
        else:
            print(f"unrecognized input: {cmd_param.split()[0]}")
```

Zugegeben, diese App kann noch nicht wirklich was.
Alles was sie bisher kann ist sich selbst zu beenden.
Alles weitere wird nur als *Unbekannte Eingabe* abgetan.
Es sollte dennoch ersichtlich sein, wie wir diesen Grundpfeiler weiter ausbauen werden.
Mit jeder neuen Funktion werden wir einen weiteren `if`-Block hinzufügen können, welcher unsere Eingaben bearbeitet.

**main.py**

```python
"""
Starting Point of the CLI
Programm to import and de-duplicate files from an external device
"""

if __name__ == "__main__":

    while True:
        cmd_param = input("$: ")
        if cmd_param.split()[0] == "quit":
            quit()
        else:
            print(f"unrecognized input: {cmd_param.split()[0]}")
```
