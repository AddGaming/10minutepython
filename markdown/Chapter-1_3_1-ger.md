# Kapitel 1: Grundlagen

## Kapitel 1.3: Iteration und Datenmengen

### Kapitel 1.3.1: Listen & Tupel

Oft wird es vorkommen, dass man mehrere Datenpunkte hat, statt nur einen.   
Um nochmal auf das Beispiel von gestern zurückzukommen:   
Ich will nicht nur mein Zimmer, sondern die Fläche meines Hauses ausrechnen.   

Falls ich das machen will, brauche ich einen Weg alle Räume sinnvoll zu repräsentieren.   
Ein naiver Ansatz wäre der Folgende:

```python
>>> laenge_raum_1 = 4
>>> breite_raum_1 = 5
>>> laenge_raum_2 = 6
>>> breite_raum_2 = 7
>>> ...
```

Die Nachteile sind hoffentlich ersichtlich.   
Daher bietet Python viele Wege an, mehrere *Datenpunkte* in einer *Datenstruktur* zu gruppieren.   
Zwei davon werden wir heute kennenlernen.

Eine Liste ist jedem von uns aus dem Alltag bekannt.   
Man kann Sachen auf eine Liste schreiben und danach wieder von ihr ablesen.   
In einer To-do-Liste werden sogar oft Elemente wieder entfernt, indem sie durchgestrichen werden.   
Listen in Python verhalten sich ähnlich.   
Wir erstellen eine neue Liste auf folgende Art und Weise:

```python
raeume = list()
```

`list()` gibt an, dass es sich um eine Liste eignet.
Die Liste ist etwas leer, also füllen wir sie einmal:

```python
>>> raeume = list()
>>> raeume.append("Küche")
>>> raeume.append("Wohnzimmer")
>>> raeume.append("Schlaftimmer")
```

Wenn wir uns jetzt anschauen, was in der Liste drin ist, erhalten wir die Elemente, die wir vorher reingesteckt haben.

```python
>>> raeume
['Küche', 'Wohnzimmer', 'Schlaftimmer']
```

> **Aufgabe:**    
> Erstelle eine neue Liste und befülle sie mit Zahlen oder mit Zeichenketten.

Also können wir einer Liste mit `append()` neue Elemente hinzufügen.    
Analog geht es für das Entfernen von Elementen:    

```python
>>> raeume.remove("Wohnzimmer")
>>> raeume
['Küche', 'Schlaftimmer']
```

Mit dem *Schlüsselwort/Keyword* `remove` können wir Elemente wieder aus einer Liste entfernen.    
Nun haben Listen noch eine sehr verwandte Datenstruktur; das Tupel.    
Ein Tupel wird ähnlich wie eine Liste erstellt.

```python
>>> mein_tupel = tuple()
>>> mein_tupel
()
```

Wie erwartet ist das Tupel leer.    
Tupel unterscheiden sich in der Darstellung und nutzen `()` anstatt der `[]` von Listen.    
Falls wir nun aber versuchen sollten, das Tupel anzupassen, wie wir es mit Listen getan haben, werden wir nicht weit kommen...   

```python
>>> mein_tupel.append(1)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'tuple' object has no attribute 'append'
```

Und das ist auch der zweite Unterschied zwischen Tupel und Listen.   
Tupel sind unveränderbar.   
Nun kann die Frage aufkommen, wo das bitte ein nützliches verhalten sein soll.    
Die Antwort ist vielleicht etwas zu simpel; dort wo sich Daten nicht verändern sollen.   

Um zum Haus-Beispiel zurückzukommen

```python
>>> raeume = [(4,5), (6,7), (2,2)]
>>> raeume
[(4, 5), (6, 7), (2, 2)]
```

Hier habe ich die Räume als Kombination von Listen und Tupel definiert.    
Die Anzahl der Räume ist somit variabel. Ich kann Räume hinzufügen, oder entfernen.    
Dabei bleiben allerdings die Größen der Räume immer konstant und unveränderbar.    

Wir haben es also mit einer Liste an Räumen zu tun.    
Der erste Raum in dieser Liste ist 4 lang und 5 breit.     
Der zweite Raum ist 6 lang und 7 breit.   
...    

Wie ich gestern versprochen hatte, kommt jetzt der Punkt, wo sich Funktionen, Variablen und Listen zu einem mächtigen Werkzeug entwickeln.   
Wie können über Listen *iterieren*.    
Sprich, wir können die m^2 eines jeden Raumes ausrechnen, ohne viel Arbeit.

```python
>>> for laenge, breite in raeume:
...   print(flaeche(laenge, breite))
...
20
42
4
```

Whoa, da ist grade viel passiert. Brechen wir also den Code Stück für Stück runter.   
Die erste Zeile lässt sich praktisch 1 zu 1 auf Deutsch übersetzen.   
*Für raum in räume*, mach das Folgende. In unserem konkreten Fall besteht ein Raum aus Länge und Breite.    
Nun markieren wir mit Einrückung, dass die folgenden Zeilen alle Teil der *for-Schleife* sind.    
`print()` gibt uns wie immer den Inhalt der Klammern als Text auf der Consol aus.    
`flaeche()` ist die Funktion von gestern, welche uns die Quadratmeter ausrechnet.    
Und `laenge` und `breite` sind die beiden *Parameter* welche von unserer Flächen-Funktion erwartet werden.

Also rufen wir `flaeche()` mit `laenge` und `breite` auf und geben uns das ergebnis mit `print()` auf der Consol aus.

Vielleicht ist die Leseweise am Anfang etwas verwirrend, aber mit jeder Funktion die ihr aufruft wird es einfacher werden, den Inhalt zu erkennen.    
Üben wir das ganze direkt noch einmal.

> **Aufgabe:**    
> Schreibe eine Schleife, welche aus der folgenden Liste an Länge, Breite, Höhe das Volumen der einzelnen Räume ausrechnet.    
> Gebe das Ergebnis auf der Consol aus.    
> Liste der Räume:    
> `[(11, 8, 14), (11, 18, 10), (13, 18, 11), (15, 4, 12), (19, 10, 20), (7, 10, 2), (1, 11, 20), (19, 1, 13), (4, 13, 10), (14, 1, 17), (17, 15, 12), (6, 2, 13), (14, 1, 18), (9, 15, 18), (2, 15, 1), (6, 10, 6), (9, 1, 10), (9, 19, 19), (7, 18, 10), (16, 13, 5), (8, 10, 5), (14, 15, 6), (14, 16, 20), (13, 20, 4), (19, 5, 4), (15, 3, 9), (15, 14, 20), (9, 1, 2), (7, 1, 10), (19, 8, 6), (5, 13, 12), (16, 11, 7), (13, 3, 16), (7, 17, 8), (2, 16, 9), (10, 11, 18), (5, 1, 5), (11, 4, 5), (12, 10, 1), (15, 12, 4), (7, 7, 12), (12, 8, 4), (12, 20, 2), (2, 19, 18), (12, 16, 12), (9, 15, 3), (17, 15, 7), (12, 5, 13), (12, 4, 19), (7, 6, 16), (6, 3, 11), (12, 15, 5), (5, 10, 16), (6, 7, 14), (19, 10, 5), (6, 19, 9), (20, 8, 7), (12, 5, 12), (8, 11, 3), (20, 4, 8), (3, 3, 16), (19, 3, 5), (17, 13, 16), (19, 4, 10), (16, 14, 6), (9, 1, 3), (15, 13, 3), (1, 12, 12), (13, 17, 18), (17, 11, 6), (16, 16, 13), (6, 18, 14), (19, 19, 10), (9, 5, 18), (2, 19, 11), (4, 12, 6), (7, 14, 12), (11, 19, 17), (1, 8, 5), (2, 7, 4), (14, 9, 16), (13, 11, 9), (6, 6, 17), (20, 9, 1), (11, 11, 19), (4, 1, 10), (16, 5, 4), (17, 2, 17), (4, 5, 10), (11, 8, 8), (16, 10, 7), (16, 20, 1), (9, 3, 20), (15, 15, 18), (12, 4, 3), (5, 9, 11), (5, 17, 20), (15, 5, 8), (13, 19, 10), (20, 5, 15)]`


Ich hoffe das der Wert dieser Art der Iteration an der letzten Aufgabe erkennbar geworden ist.   
Auch wenn noch viel zu Listen und Tupeln ungesagt blieb, hoffe ich dennoch, dass dies eine nützliche Einführung war.   
