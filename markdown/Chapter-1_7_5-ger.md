# Kapitel 1: Grundlagen

## Kapitel 1.7: Entwicklungspraktiken

### Kapitel 1.7.5: Wann sollte ich programmieren?

Nun da du in der Lage bist deine eigenen Programme zu schreiben, stellt sich noch die Frage,
wann du deine neu erworbenen Fähigkeiten benutzen solltest.

Wenn dir auffällt, dass du immer und immer wieder das Selbe machst,
kann klingt das stark nach einem Problem wo Programmieren helfen kann.  
Beispiele dafür wären Probleme, die oft mit großen Datenmengen einhergehen:

- Dateien umbenennen
- etwas in einem Datensatz suchen
- wiederholt eine ähnliche Matheaufgabe lösen
- ...

Dann gibt es eine weitere Klasse Probleme, bei dehnen es sich zwar anbietet zu Programmieren,
man aber aufpassen sollte.  
Das sind alle Aufgaben, bei dehnen der Zeitgewinn durch die Automatisierung vernachlässigbar ist:

- Brauch es die sich selbst-regulierende Heizung, oder reichen manuelle Thermostate?
- Braucht es die App, welche automatisch die Youtube Videos deiner abonnierten Kanäle runterlädt,
  oder konsumierst du am Ende die Videos doch auf der Platform?
- ...

Und dann gibt es Aufgaben, bei dehnen man noch gar nicht weiß, wie oft man sie machen muss:

- Ein Bild bearbeiten (meistens Unikat-Arbeit)
- Ein Lied schreiben
- ...

Ich kann auch nur vom so genannten "*cracking*", dem Knacken von Computersystemen, abraten.   
Auch wenn dies viel Spaß machen kann,
kann ein unerfahrener Hacker sich zu schnell eine Geldstrafe oder schlimmeres einfangen.

Ein einfacher Ansatz um die verschiedenen Fälle zu unterscheiden,
ist sich die investierte Zeit und die Ergebnisse anzuschauen.    
Für die Mustererkennung braucht es etwas Übung, jedoch nicht viel.

__________________

Dieses Kapitel war sehr kurz, aber meiner Meinung nach dennoch essenziell.   
Das nächste Kapitel ist ähnlich Kurz.
Falls du also noch motiviert bist, kannst du Heute das Buch beenden.