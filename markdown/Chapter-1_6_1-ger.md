# Kapitel 1: Grundlagen

## Kapitel 1.6: Files

### Kapitel 1.6.1: Programme speichern und effektiv entwickeln

Bisher haben wir nur in der Consol programmiert.    
Ich habe nicht wie andere Kurse Programm-Files als eines der esten Dinge eingeführt,   
weil ich bestimmte Programmiergewohnheiten fördern wollte.   
Aber die Nachteile an reinem Consol-Programming sind deutlich.   
Wir können beispielsweise nicht unsere Programme speichern und wiederverwenden.

Das sollte sich ändern.    
An sich können wir unsere Programme einfach in text-files schreiben.   
Schauen wir uns dafür das Beispiel aus 1.4.3 an:

```
E:\>py asia.py
こんにちは
```

Hier führe ich die Datei (eng. `file`) mit python aus.   
Es fällt auf, das die Datei in `.py` endet. Das ist jedoch kein Muss.

```
E:\>py asia.txt
こんにちは
```

Python ist auch damit zufrieden, wenn wir ihm eine `txt` geben, solange es ein valides Python programm beinhaltet.

Ok, Problem gelöst, wir programmieren einfach in einer `txt` und führen es danach über die Konsole aus.   
Auch wenn dieser Ansatz vielleicht verlockend wirkt, birgt er doch viele Probleme.   
Diese wirst du spätestens dann merken, wenn wir mehrere hundert Zeilen Code geschrieben haben    
und sich ein Bug in diesen versteckt.   
Ein besserer weg Software zu entwickeln, ist entweder ein Text-Editor mit Code unterstützung, oder eine IDE.   
Ein Text-Editor ist wahrscheinlich selbsterklärend, aber was ist eine IDE?   
Ein *Integrated Development Environment* ist ein vorkonfigurierter Werkzeugkasten, der mit viel Funktionalität kommt,   
um das programmieren so angenehm zu machen, wie es nur geht.    
Es gibt für sowohl Text-Editoren als auch IDEs kostenlose und kostenpflichtige Varianten.

Ich empfehle als IDE "Pycharm" welches in der [Community-Edition](https://www.jetbrains.com/pycharm/download/)
kostenlos ist.   
Als Text-Editor empfehle ich [VSCode](https://code.visualstudio.com/download) welches aufgrund seines hohen Grades der Anpassbarkeit eine große Beliebtheit
in bei Programmieren genießt.

Aber wie sollte ich meine Programme abspeichern?   
Grundsätzlich sollten Programme in 2 Kategorien unterteilt werden.

1. Große Projekte
2. Kleine Skripte

Große Projekte sollten ihren eigenen Ordner bekommen in welchen du die Dateien sorgsam sortierst.   
Mehr dazu im nächsten Kapitel.    
Für kleinere Skripte reicht es aus, wenn du dir einen Sammelordner machst in welchem du diese abspeicherst.   
Die Wahrscheinlichkeit, dass du ein Skript mehr als ein mal ausführst, ist klein.   
Falls du dennoch bemerkst, wie du ein Skript immer und immer wieder benutzt,     
solltest du aus dem Skript ein vollwertiges Projekt machen.

Nutzen wir doch das neue Wissen über Dateien aus, und schreiben unser heutiges Programm in einer Datei!    
Da ich jedoch aus Platzgründen, nicht immer eine ganze Datei in den Fließtext einbinden kann,     
befindet sich ab diesem Kapitel am ende des Textes eine Datei mit den Inhalten den Code des Kapitels.

Aber bevor wir mit der eigentlichen Aufgabe loslegen können, muss ich noch eine neue Funktion vorstellen: `input()`

> Heute schreiben wir ein kleines Spiel.   
> Schreibe ein Programm, welches zufällig eine Zahl von 1 bis 10 auswählt,   
> welche der Nutzer dann erraten kann.    
> Wenn der Nutzer die richtige Zahl erraten hat, endet das Programm.

Ok, brechen wir diese Aufgabenstellung nun Schritt für Schritt runter.    
Als erstes sehen wir, dass wir *zufällig eine Zahl von 1 bis 10* im Text stehen haben.   
Das stellt uns vor die Wahl: Schreiben wir unseren eigenen Zufall,
oder können wir einen Zufall nutzen, den Python bereits kennt?    
In diesem Fall nutzen wir den Vorimplementierten Zufall den Python uns zur Verfügung stellt.

```python
import random
```

Nun können wir mit Hilfe des vom `random` zufällig Zahlen generieren.

```python
x = random.randint(1, 10)
```

Hier generieren wir eine zufällige Zahl zwischen 1 und 10 und weisen diese der Variable `x` zu.   
Dafür wird die funktion `randint()` benutzt.   
Der Name leitet sich von `random` (Zufall) und `integer` (Ganzzahl) ab.    
Die Parameter sind die untere und die obere Grenze.

Jetzt wo wir bereits unsere Zufallszahl haben, können wir uns um den nächsten Teil der Aufgabenstellung kümmern.   
"*welche der Nutzer dann erraten kann*"   
Dafür benutze ich im nächsten schritt die bereits erwähnte `input()` Funtion:

```python
while int(input("dein Tip: ")) != x:
    print("leider falsch")
```

Oha, da passiert aber ziemlich viel in einer Zeile.   
Gehen wir das ganze Schritt für Schritt durch:

Zuerst sehen wir, dass es sich um eine `while`-Schleife handelt.   
Das bedeutet, dass das Programm so lange läuft, solange die Bedingung `True` ist.

Die Bedingung ist ein "!=". Das heißt, dass beide Elemente unterschiedlich sein müssen, damit die Bedingung wahr
ist.    
Auf der einen Seite sehen wir das `x`, unsere zufällig gewählte Zahl.    
Auf der anderen Seite ein Verschachtelter Ausdruck.   
Der äußerste Teil ist ein Aufruf der `int()` Funktion, welche einen Text in eine Zahl umwandelt.    
Den Text der umgewandelt werden soll, erhalten wir von der `input()` Funktion.   
Diese gibt zuerst den, als Argument übergebenen, Text aus, und gibt dem Programm die Eingabe des Nutzers zurück.   
Wichtig dabei ist, dass ein Nutzer seine Eingabe mit der `Enter`-Taste bestätigen muss.

Das Programm ist jetzt fast Fertig.   
Fehlt nur noch die Siegesnachricht.

```python
print("Yay, Richtig!")
```

Somit erhalten wir unser Fertiges Programm:

```python
import random

x = random.randint(1, 10)
while int(input("dein Tip: ")) != x:
    print("leider falsch")
print("Yay, Richtig!")
```

Kopiere es ruhig in eine Datei uns Spiel etwas damit rum:

```
C:\Zwischenspeicher>py guessNumber.py
dein Tip: 1
leider falsch
dein Tip: 9
leider falsch
dein Tip: 10
Yay, Richtig!
```

Mache dir bitte klar, wie das Programm funktioniert.   
Die folgende Aufgabe hilft dir Dabei:

**Aufgabe:**
> Das Spiel von heute ist noch etwas langweilig.    
> Was ist, wenn wir größere Zahlen nehmen wollen? Dann wird das Spiel ja unspielbar!   
> Erweitere das Spiel also nun um Rückmeldung.    
> Anstatt nur ein "leider falsch", gib ein "größer" zurück, wenn x größer ist, als die geschätzte Zahl   
> und ein "kleiner", wenn x kleiner ist, als die geschätzte Zahl.
> Beispiel Ausgabe:
>> C:\Zwischenspeicher>py guessNumber.py    
> > größer     
> > dein Tip: 1      
> > größer     
> > dein Tip: 10      
> > kleiner     
> > dein Tip: 5        
> > größer     
> > dein Tip: 6     
> > Yay, Richtig!    

