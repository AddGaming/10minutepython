# Kapitel 1: Grundlagen

## Kapitel 1.3: Iteration und Datenmengen

### Kapitel 1.3.2: Dictionaries & Sets

Ähnlich zu gestern, geht es heute wieder um eine Datenstruktur, welche es uns ermöglicht mehrere Datenpunkte in einer Variabel zu speichern.   
Wir haben bereits Listen und Tupel kennengelernt.   
In Listen konnten wir dynamisch Elemente hinzufügen und in Tupel nicht.    
Heute lernen wir zwei weitere dynamische Typen kennen: Dictionaries und Sets.

Schauen wir uns nochmal das erste Beispiel von gestern an:

```python
>>> raeume = list()
>>> raeume.append("Küche")
>>> raeume.append("Wohnzimmer")
>>> raeume.append("Schlaftimmer")
>>> raeume
['Küche', 'Wohnzimmer', 'Schlaftimmer']
```

Ein potentielles Problem könnte sein, dass wir jederzeit durch Duplikate Verwirrung stiften können.

```python
>>> raeume.append("Küche")
>>> raeume.append("Küche")
>>> raeume.append("Küche")
>>> raeume
['Küche', 'Wohnzimmer', 'Schlaftimmer', 'Küche', 'Küche', 'Küche']
```

Welche `Küche` jetzt welcher Raum ist unklar.    
Eine befindet sich vielleicht im Erdgeschoss, eine andere im ersten Stockwerk, eine andere im zweiten Stockwerk, ...    
Um Duplikate zu entfernen, könnten wir komplizierte und langsame Algorithmen schreiben,     
oder wir nehmen direkt den richtigen Datentyp.    
Das *Set* garantiert uns, dass jedes Element nur ein einziges mal enthalten ist.

```python
>>> raeume = set()
>>> raeume.add("Küche")
>>> raeume.add("Wohnzimmer")
>>> raeume.add("Schlaftimmer")
>>> raeume
{'Schlaftimmer', 'Wohnzimmer', 'Küche'}
>>> raeume.add("Küche")
>>> raeume.add("Küche")
>>> raeume
{'Schlaftimmer', 'Wohnzimmer', 'Küche'}
>>>
```

Im Beispiel füge ich `"Küche"` mehrfach den `raeume` hinzu.    
Am ende enthält `raeume` dennoch nur eine `"Küche"`.    
Ein Nachteil an Sets und Dictionaries ist jedoch, dass sie nicht mehr geordnet sind.    
Wir wissen also nicht, ob das Element was wir als erstes eingefügt haben, auch als Erstes wieder herausbekommen.    

Konkret aus dem Beispiel: Wir fügen als erstes `"Küche"` hinzu.    
Aber bei der Ausgabe ist `"Küche"` das letzte Element.    
`"Schlafzimmer"` fügen wir als letztes hinzu, dennoch finden wir es als erstes Element in der Ausgabe.   

Somit sollte immer abgewogen werden, ob man die *Einzigartigkeit* von Sets benötigt.

> **Aufgabe**:    
> Erstelle ein Set an Süßigkeitennamen und gebe es dir auf der Consol aus.

Betrachten wir nun nochmal ein weiteres Beispiel von Gestern:

```python
raeume = [(4,5), (6,7), (2,2)]
```

Ein Problem das wir hier haben, ist dass uns die Beschreibung fehlt, um welchen Raum es sich handelt.    
Klar könnten wir das lösen indem wir eine zweite Liste mit Deskriptoren verwenden, aber Python hat eine elegantere Lösung. 
Ein Dictionary besteht aus *Schlüssel-Wert-Paaren* oder auf Englisch; *Key-Value-Pairs*. 

```python
>>> raeume = dict()
>>> raeume["Küche"] = (4,5)
>>> raeume["Wohnzimmer"] = (6,7)
>>> raeume["Schlafzimmer"] = (2,2)
>>> raeume
{'Küche': (4, 5), 'Wohnzimmer': (6, 7), 'Schlafzimmer': (2, 2)}
```

Wir legen im Dictionary `raeume` unter dem *Schlüssel* `"Küche"` als *Wert* ein Tupel mit der Größe des Raumes an.     
In der Ausgabe werden *Key* und *Value* mit einem `:` getrennt und einzelne *Key-Value-Pairs* mit `,`.    

Auch hier gilt das Gebot der Einmaligkeit und der unbekannten Reihenfolge.    
Jeder Schlüssel darf nur ein einziges mal im Dictionary vorkommen.    
Sollte man mehrfach auf den selben Schlüsselwert schreiben wird der Wert des Schlüssels jedes mal überschrieben.     
Gleichzeitig wissen wir nicht, in welcher Reihenfolge die Schlüssel gespeichert werden.    
Das ist bei Dictionaries aber weniger ein Problem als bei Sets, weil wir hauptsächlich auf den Schlüsseln arbeiten.   

Wir können genauso über Dictionaries und Sets iterieren, wie wir es gestern bei Tupel und Listen gelernt haben:

```python
>>> mein_set = set()
>>> mein_set.add(1)
>>> mein_set.add(2)
>>> mein_set.add(3)
>>> for element in mein_set:
...   print(element)
...
1
2
3
```

Blos bei Dictionaries brauch es etwas mehr Arbeit...

```python
>>> for key, values in raeume.items():
...   print("Schlüssel: " + key + " | Wert: " + str(values))
...
Schlüssel: Küche | Wert: (4, 5)
Schlüssel: Wohnzimmer | Wert: (6, 7)
Schlüssel: Schlafzimmer | Wert: (2, 2)
```

Falls das Iterieren auf Dictionaries etwas unklar ist im Moment, ist das ok.
Ich werde näher darauf eingehen, wie es funktioniert, wenn wir es in einem praktischen Projekt brauchen.
Für den Moment reicht es, wenn du dir merkst, dass man über Dictionaries mit einem *for-loop* iterieren kann.

> **Aufgabe**:    
> Erstelle nun ein Dictionary an Süßigkeiten.    
> Die Namen sind in diesem Fall die Schlüssel     
> und die Werte sind Zahlen auf einer Skala von 1 bis 10 wie sehr du die Süßigkeit magst.
