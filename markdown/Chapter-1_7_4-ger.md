# Kapitel 1: Grundlagen

## Kapitel 1.7: Entwicklungspraktiken

### Kapitel 1.7.4: English is the new German & Kommentare

Eine Sache, welche ich ab Kapitel 2 *nicht* mehr machen werde, ist auf "Deutsch" zu programmieren.

Sowohl die Schlüsselwörter als auch alles um Programmiersprachen herum ist auf Englisch.   
Deshalb hat es sich als standard etabliert, auch nur Englisch in Programmdateien zu schreiben.

Ich habe diese Konvention absichtlich an vielen stellen gebrochen, um den Einstieg auch Leuten zu ermöglichen,  
welche kein Englisch sprechen.   
Das wird jedoch ab Kapitel 2 sehr schwer werden.

Immer mehr Artikel und Dokumentationen die ich verlinke werden auf englischer Sprache sein.    
Immer weniger Resourcen auf Deutsch werden auffindbar.   
Und auch wenn ich mir sicher bin, dass es viele talentierte deutsche Programmierer gibt,
sprechen auch diese beim Programmieren nur Englisch.

Ein weiterer Grund, weshalb ich besonders in einer Sprache wie Python empfehle,
auch freiwillig in Englisch zu programmieren ist, weil die Sprache so programmiert werden kann,
dass es sich fast wie ein flüssiger englischer Satz anhört, wenn man es laut vorliest.

Dies macht es sowohl der Person, die den Code schreibt,
als auch zukünftigen lesern wesentlich einfacher den Code zu verstehen.    
Diese inherente Stärke der Sprache weg zu werfen und in einer komischen Deutsch-Englisch Mischung zu programmieren,
erscheint mit als rückschrittig.

Falls du der das hier liest nicht des Englischen mächtig bist, empfehle ich dir dringend es zu lernen.   
Auch wenn du vorerst ohne große englisch Kenntnisse weiter machen können wirst,
wird es nur vorteile haben, diese Sprache zu lernen.

Der Ort an dem man jedoch Programmiersprachen unabhängig am ehesten vollständiges Englisch sehen wird,
ist in Kommentaren.    
Es gibt 2 Arten Programmcode in Python zu Kommentieren.

1. Mit Hilfe eines `#`. Alle Zeichen in der Selben Zeile werden nach dem `#` ignoriert.
2. Man kann einen Kommentarblock mit `"""` öffnen und wieder schließen.

Aber Moment mal, das ist dann ja aber nur ein normaler `String`?! - Genau so ist es.   
Python führt auch diese Code Zeile aus, jedoch hat diese einfach keinen Effekt.   
Aber keine Angst. Einen negativen Effekt auf die Programmlaufzeit hat das Kommentieren nicht.

Warum sollte ein Programmierer jetzt also Kommentare schreiben?   
Weil manche details im Code alleine nicht erkennbar werden.   
Beispielsweise: Warum wird hier (in dieser imaginären Codezeile) eine Liste und kein Tupel benutzt?

Dies sind Dinge, welche ich persönlich sehr schnell vergesse, weshalb ich ohne Kommentare wahrscheinlich
viel zu viel Zeit damit verbringen würde, Dinge zu testen, die ich bereits ausprobiert habe.

Wann und was man am besten Kommentiert, kann ich leider nicht genauer spezifizieren.   
Das kommt über Erfahrung. Sowohl die eigene, was man selbst gerne lesen würde, als auch, was andere gerne wissen würden.

Ich hoffe ich kann in Kapitel 2 in den Projekten eine Intuition vermitteln, wie effektiv Kommentiert werden kann. 