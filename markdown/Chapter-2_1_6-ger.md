# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

### Kapitel 2.1.6: Ein- und Auspacken & Documentation

Bevor wir mit dem Stern anfangen, will ich noch Zeigen, warum wir immer so brav aufgeschrieben haben,
was unsere Funktionen machen, und wie.

Ich hab zwar im letzten Kapitel gesagt, dass es schön wäre wenn sowohl "quit" als auch "exit" funktioniert,
aber wäre es nicht praktisch zu wissen, was die App überhaupt kann?
Um das momentan rauszufinden müssen wir in den Code schauen.
Das ändern wir jetzt.

In Python heißt der Text unterhalb der Definition einer Funktion oder Klasse "`doc-string`".
Soweit hoffentlich nichts neues.
Was ich jedoch noch nicht verraten habe, war das wir diesen Text in unserem Programm auslesen können.

```python
>>> def test_fun():
...   """prints a test string"""
...   print("my test")
...
>>> test_fun.__doc__
'prints a test string'
```

Wir sehen, dass der Doc-String unter `__doc__` abgespeichert wurde.
Um nun auszugeben, was unsere App alles kann, müssen wir nur über unser Dictionary iterieren
und jeweils die entsprechende `__doc__` Methode aufrufen.

```python
def give_help() -> None:
    """
    prints the documentation of all registered functions to the screen.
    :return: None
    """
    for key, val in registered.items():
        print(f"{key}: {val.__doc__}")
```

Schreiben wir nun die Funktion in unsere `CLI/__init__.py` und aktivieren die Hilfe in unserem Dictionary:

```python
registered = {
    "import": copy_from,
    "tree": print_tree,
    "help": give_help,
    "quit": quit,
    "exit": quit,
}
```

Probieren wir doch gleich die neue Funktion aus!

```
$: help
import: 
    Copies all elements fitting the endings descriptor into a destination folder.
    Traverses subdirectories recursively.
    :param source: str - source path
    :param destination: str - destination path
    :param endings: [str] - list of endings
    :raises RuntimeError: if source or destination are not folders
    :return: None
    
tree: 
    prints the structure of a given folder | if path leads to file, only file path is printed
    :param path: str - the path
    :param depth: int - current recursion depth
    :return: None
    
help: 
    prints the documentation of all registered functions to the screen.
    :return: None
    
quit: None
exit: None
```

Das funktioniert ja super. Jetzt können wir an einem Ort aufschreiben wie die Funktion funktioniert,
und wir können überall auf diese Dokumentation zugreifen.
Zwar ist der "None" - Text bei "quit" und "exit" etwas störend, aber auch dass wird sich bald ändern.

Nun aber wie Versprochen zum Stern-Operator.
Wenn wir Funktionen aufrufen, geben wir dehnen meisterns mehr als nur einen Parameter mit.
Was aber, wenn wir wie in unserem konkreten fall alle Parameter in einer Liste haben?
Wenn wir die gesamte Liste einfach als solche eingeben würden, würde sie auch nur als 1 Parameter wahrgenommen werden.

```python
>>> def test_fun(p1,p2):
...   print(p1,p2)
...
>>> l = [1,2]
>>> test_fun(l)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: test_fun() missing 1 required positional argument: 'p2'
```

Mit dem Stern können wir diese Problem beheben.
Der Stern packt einfach alle Elemente in der Liste einzeln in die Funktion.
Deshalb wird das auch als *unpacking* (de. auspacken) bezeichnet.

```python
>>> test_fun(*l)
1 2
```

Aber nun haben wir ein neues Problem.
Was ist, wenn unsere Liste nun zu viele Elemente hat?

```python
>>> l = [1,2,3,4]
>>> test_fun(*l)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: test_fun() takes 2 positional arguments but 4 were given
```

Hmm, das sieht nicht so gut aus. Wie können wir nun verhindern, dass uns zu viele Argumente die Suppe versalzen?
Da gibt es zwei prevalente Möglichkeiten für.
Die erste ist ein If-Statement, welches überprüft, dass nicht zu viele Elemente überreicht worden sind.
Das gibt uns die möglichkeit diesen Fall in der Funktion selbst zu behandeln.
Was aber, wenn uns der Rest einfach egal ist?
Dann können wir den einfach wieder einpacken.
Das geht auf die selbe Art und Weise, wie wir die Parameter ausgepackt haben.

```python
>>> def test_fun(p1,p2, *args):
...   print(p1,p2)
...   print(args)
...
>>> test_fun(*l)
1 2
(3, 4)
```

Wir sehen, dass alle überschüssigen Argumente in ein Tupel eingepackt worden sind.
Also können wir nun Listen, Tupel und Sets auspacken und wieder einpacken.
Aber was ist mit Dictionaries?
Wir können eine ähnliche Funktion auch auf Dictionaries benutzen, solange wir einen weiteren Stern vorne dran setzen:

```python
>>> def test_fun(p1=1,p2="hi",**kwargs):
...   print(p1)
...   print(p2)
...   print(kwargs)
...
>>> d = {"p1": 5, "p2": "good bye", "p3": 123}
>>> test_fun(**d)
5
good bye
{'p3': 123}
>>>
```

Jetzt, da wir verstanden haben, wie Ein- und Auspacken funktioniert,
können wir auch die Zeilen 13-15 in der Main verstehen.

```python
cmd_param = input("$: ")
if cmd_param.split()[0] in registered:
    registered[cmd_param.split()[0]](*cmd_param.split()[1:])
```

Zuerst fragen wir nach der Nutzereingabe uns speichern diese in der Variabel "cmd_param".
Danach schauen wir, ob wir in unserem Dictionary eine Funktion haben, die als Schlüssel den Text hat,
den der Nutzer vor dem ersten Leerzeichen eingegeben hat.
Falls wir einen entsprechenden Eintrag gefunden haben, rufen wir die Funktion an diesem Schlüssel auf.
Als Parameter geben wir die restlichen Eingaben des Nutzers weiter.

Das wars auch wieder für dieses Kapitel.
In dem nächsten Kapitel schauen wir uns einen Weg an, wie wir das Importieren der Dateien nun funktionstüchtig bekommen.