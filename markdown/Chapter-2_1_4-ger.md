# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

### Kapitel 2.1.4: Kopieren

Jetzt da alle Grundlagen gelegt sind, können wir endlich anfangen die Dateien zu kopieren.
Schreiben wir also eine Funktion `copy_from()`,
welche als Grundgerüst die rekursive Struktur aus dem letzten Kapitel hat:

```python
def copy_from(path: str) -> None:
    if os.path.isdir(path):
        for element in os.listdir(path):
            element_path = f"{path}{os.sep}{element}"
            if os.path.isdir(element_path):
                copy_from(element_path)
    else:
        print(path)
```

Der Code ist fast identisch zu dem von Gestern.
Ich habe lediglich den Namen der Funktion geändert und die `print`s entfernt.
Das erste was wir nun machen, ist das erste `if-else` auflösen.
Was wir damit machen wollten, war den Fall abfangen, bei dem wir eine Datei und keinen Pfad als Parameter bekommen.
Also machen wir aus der fehlerhaften Eingabe einen Fehler:

```python
def copy_from(path: str) -> None:
    if not os.path.isdir(path):
        raise RuntimeError("The source path is not a folder")

    for element in os.listdir(path):
        element_path = f"{path}{os.sep}{element}"
        if os.path.isdir(element_path):
            copy_from(element_path)
```

Ok, nun bauen wir als nächstes die Kopierfunktionalität ein.
Dafür benutze ich die Funktion `copy2()` aus der Bibliothek `shutil`.
Der unterschied zwischen `copy()` und `copy2()` ist hierbei, dass `copy2()` die Zeitstempel mit kopiert.

```python
def copy_from(path: str) -> None:
    if not os.path.isdir(path):
        raise RuntimeError("The source path is not a folder")

    for element in os.listdir(path):
        element_path = f"{path}{os.sep}{element}"
        if os.path.isdir(element_path):
            copy_from(element_path)
        else:
            shutil.copy2(element_path, destination)
```

Dir ist sicherlich aufgefallen, dass zum kopieren 2 Parameter benötigt werden.
Einmal das zu kopierende Element und einmal das Ziel wohin es kopiert werden soll.
Da wir das Ziel noch nicht als Parameter in unserer Funktion haben, sollten wir die Signatur anpassen:

```python
def copy_from(source: str, destination: str) -> None:
    if not os.path.isdir(source) or not os.path.isdir(destination):
        raise RuntimeError("The source path is not a folder")

    for element in os.listdir(source):
        element_path = f"{source}{os.sep}{element}"
        if os.path.isdir(element_path):
            copy_from(element_path, destination)
        else:
            shutil.copy2(element_path, f"{destination}{os.sep}{element}")
```

Nun wollen wir jedoch seltenst einfach alles aus einer Ordnerstruktur in einen anderen Ordner kopieren.
Meistens, wenn wir ein Handy oder USB-Stick anschließen um ein Backup zu machen,
interessieren uns nur Musik oder Bilder.
Also sollten wir auch einen Filter in unsere Kopierfunktion einbauen, dass nur bestimmte Inhalte kopiert werden.

```python
def copy_from(source: str, destination: str, endings: [str]) -> None:
    if not os.path.isdir(source) or not os.path.isdir(destination):
        raise RuntimeError("The source path is not a folder")

    for element in os.listdir(source):
        element_path = f"{source}{os.sep}{element}"
        if os.path.isdir(element_path):
            copy_from(element_path, destination, endings)
        else:
            for ending in endings:
                if element.endswith(ending):
                    shutil.copy2(element_path, f"{destination}{os.sep}{element}")
                    break
```

Das `break` welches momentan in der letzten Zeile zu finden ist, ist neu.
Oft kommt es vor, dass wir beim iterieren über etwas nur bis zu einer bestimmten stelle Suchen.
In diesem fall wollen wir wissen, ob eine der Endungen zu der Datei passt.
Sobald wir die erste passende Endung gefunden haben, brauchen wir die anderen nicht mehr zu durchsuchen.
Wir haben schließlich schon die Entsprechende gefunden.
`break` unterbricht nun die Schleife.

Nehmen wir als beispiel an, dass wir die Endungen `endings = [".png",".jpg",".mp3"]` haben und das Element
`element = "haus.png"`.
Dann würde unsere Schleife nach dem ersten Vergleich abbrechen, weil wir bereits die Antwort gefunden haben.
Für `element = "the_black_parade.mp3"` müssten wir jedoch alle Endungen durchprobieren.

Nun sollten wir uns jedoch noch um eine weitere Sache kümmern bevor wir mit diesem Kapitel fertig sind.
Was passiert, wenn wir ein Duplikat haben?
Also im Zielordner bereits ein Element mit dem selben Namen existiert?
Das kann zu fehlern führen und um das zu vermeiden gibt es verschiedene Taktiken.

Wir könnten...

- ... Duplikate einfach das Präfix "*Kopie von*" geben.
- ... Duplikate nicht kopieren.
- ... Die ältere von beiden Dateien überschreiben.
- ...

In diesem Buch gehe ich mit der zweiten Option, und kopiere Duplikate einfach nicht.
Du solltest aber bereits die Fähigkeit haben jede dieser Varianten selbst zu programmieren.
Falls du also einen anderen Ansatz lieber magst, kannst du gerne ihn selbst programmieren.
Denke nur dran, es zu kommentieren, weil du es sonnst vergisst und dich wunders,
warum der Code in diesem Buch nicht so funktioniert, wie ich es behaupte.

```python
def copy_from(source: str, destination: str, endings: [str]) -> None:
    if not os.path.isdir(source) or not os.path.isdir(destination):
        raise RuntimeError("The source path is not a folder")

    destination_list = os.listdir(destination)

    for element in os.listdir(source):
        element_path = f"{source}{os.sep}{element}"
        if os.path.isdir(element_path):
            copy_from(element_path, destination, endings)
        else:
            if element in destination_list:
                continue
            for ending in endings:
                if element.endswith(ending):
                    shutil.copy2(element_path, f"{destination}{os.sep}{element}")
                    break
```

Ich habe hier den Cousin von `break` genutzt; `continue`.
Anders als `break` wird hier nicht die Schleife abgebrochen, sondern der Rest des verbliebenen Codes wird ignoriert,
und es wird zum nächsten Element der Iteration gesprungen.

Hier ein einfaches Beispiel zur Demonstration von `continue`:

```python
>>> for i in range(20):
...     if i % 5 == 0:
...         continue
...     print(i)
...
1
2
3
4
6
7
8
9
11
12
13
14
16
17
18
19
```

Somit haben wir auch dieses Kapitel abgeschlossen.
Im nächsten Kapitel werden wir uns dann ansehen, wie wir unsere Funktion in unser Programm effektiv einbinden.

**main.py**

```python
"""
Starting Point of the CLI
Programm to import and de-duplicate files from an external device
"""

def copy_from(source: str, destination: str, endings: [str]) -> None:
    """
    Copies all elements fitting the endings descriptor into a destination folder.
    Traverses subdirectories recursively.
    :param source: str - source path
    :param destination: str - destination path
    :param endings: [str] - list of endings
    :raises RuntimeError: if source or destination are not folders
    :return: None
    """
    if not os.path.isdir(source) or not os.path.isdir(destination):
        raise RuntimeError("The source path is not a folder")

    destination_list = os.listdir(destination)

    for element in os.listdir(source):
        element_path = f"{source}{os.sep}{element}"
        if os.path.isdir(element_path):
            copy_from(element_path, destination, endings)
        else:
            if element in destination_list:
                continue
            for ending in endings:
                if element.endswith(ending):
                    shutil.copy2(element_path, f"{destination}{os.sep}{element}")
                    break

def print_tree(path: str, depth: int = 1) -> None:
    """
    prints the structure of a given folder | if path leads to file, only file path is printed
    :param path: str - the path
    :param depth: int - current recursion depth
    :return: None
    """
    if os.path.isdir(path):
        for element in os.listdir(path):
            print(depth * "|--" + element)
            element_path = f"{path}{os.sep}{element}"
            if os.path.isdir(element_path):
                print_tree(element_path, depth=depth + 1)
    else:
        print(path)

if __name__ == "__main__":
    os.chdir(os.sep.join(__file__.split(os.sep)[:-1]))
    
    while True:
        cmd_param = input("$: ")
        if cmd_param.split()[0] == "quit":
            quit()
        if cmd_param.split()[0] == "tree":
            print_tree(cmd_param.split()[1])
        else:
            print(f"unrecognized input: {cmd_param.split()[0]}")
```