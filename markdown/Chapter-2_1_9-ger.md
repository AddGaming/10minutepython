# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

### Kapitel 2.1.7: Klassen- & Statische Methoden

Heute schauen wir uns an, was `@classmethod` und `@staticmethod` genau bedeutet und wie und warum wir sie benutzen.
Aber bevor wir das machen, schauen wir uns unseren Code von letzten Kapitel noch einmal an.
Wenn wir den Code so stehen lassen würden, wären unsere Standartwerte, disfunktional und würden Fehler werfen.
Das ist natürlich unpraktisch.
Die Standartwerte dürfen zwar nichts produktives machen, aber sie sollten niemals das Programm zum Absturz bringen.
Reparieren wir das schnell, indem wir abfangen, ob bei der Objektkreierung keine Werte eingegeben worden sind:

```python
class Settings:
    ...

    def __init__(self, default_source: str = "", default_destination: str = "", file_format_list: [str] = None):
        if not default_source:
            default_source = os.sep.join(__file__.split(os.sep)[:-1])
        if not default_destination:
            default_destination = os.sep.join(__file__.split(os.sep)[:-1])
        if not file_format_list:
            file_format_list = [".png", ".jpg"]

        self.default_source = default_source
        self.file_format_list = file_format_list
        self.default_destination = default_destination
```

Jetzt da auch das behoben wurde, schauen wir uns Methoden näher an.

In Python sind Funktionen, welche an eine Klasse gebunden sind, Methoden.
Wenn wir also eine Funktion in einer Klasse definieren,
geben wir als erstes Argument in dieser Methode immer das aktuelle Objekt mit.
Dieses Verhalten können wir bei unserer `save_to_file()` Methode beobachten:

```python
class Settings:
    ...
    
    def save_to_file(self) -> None:
        dump = json.dumps(self.__dict__)
        ...
```

Dadurch, dass wir immer das aktuelle Objekt wieder mitgeben,
können wir aktionen auf dieser konkreten Instanz durchführen.
Wenn wir jedoch in unserer Methode die aktuelle Instanz referenzieren, also die Funktionalität der Funktion
unabhängig von dem Objekt ist, brauchen wir auch den Parameter `self` nicht.
Solche Funktionen nennen wir `staticmethod` (de. Statische Methoden).
In Python markieren wir solche Funktionen mit einem `@staticmethod` oberhalb der Funktionsdefinition.
Ein Beispiel dafür ist unsere `_schema()` Methode, welche unabhängig vom aktuellen Objekt uns unser Schema zurück gibt.

```python
class Settings:
    ...
    
    @staticmethod
    def _schema() -> dict:
        return {
            "type": "object",
            "properties": {
            ...
```

Wir können auch Funktionen schreiben, welche zwar die bestimmte Klasse benötigen, aber nicht die konkrete Instanz.
Methoden, welche nicht das Objekt, sondern die Klasse brauchen, nennt man `classmethod` (de. Klassen Methoden).
Diese Art von Funktion ist recht selten und kommt nur in bestimmten Szenarien vor,
jedoch haben wir das Glück ein solches Szenario bereits in unserem Code zu haben.
In unserer Funktion `__new_save()` erstellen wir ein neues Objekt der Klasse `Settings`,
weshalb wir in der Funktion keinen Zugriff auf ein bestimmtes Objekt brauchen, sondern lediglich auf die Klasse.

```python
class Settings:
    ...
    
    @classmethod
    def __new_save(cls) -> Settings:
        c = cls()
        ...
```

Wir machen es deutlich, dass wir eine Class-Method nutzen, indem wir ein `@classmethod` über die Funktionsdefinition
schreiben.
Wenn du dir Python Code von anderen Programmieren anschaust, wirst du oft verschiedene Anmerkungen
mit dem `@` über Funktionen finden.
Diese können nämlich auch selbst geschrieben werden und Programmierer sind nicht an die von Python vorgegebenen
gebunden.
Das Konzept heißt *Decorator* (de. Dekorateur).
Decorators als solche sind jedoch ein recht komplexes Konzept und mit des Anspruchsvollste was in Python
machen kann.
Deshalb werde ich eine genauere Erklärung wie Decorator funktionieren erst dann machen, wenn wir sie wirklich brauchen.
Für dem Moment reicht es aus zu wissen, wie wir die von Python bereitgestellten Decorators nutzen können.

Aber woher weiß ich jetzt, ob ich es mit einer statischen Methode, einer Klassen-Methode oder einer normalen Methode
zu tun habe, wenn ich die Methode noch nicht geschrieben habe?
Die Antwort ist, dass man es eben nicht weiß.
Das ist aber auch nicht schlimm. Wir können die Funktion schreiben, und danach feststellen, wie genau sie Funktioniert.
Um das zu Illustrieren, schreiben wir eine Funktion, welche uns alle Einstellungen ausgibt.

```python
class Settings:
    ...
    
    def print_(___) -> ____:
        ...
        return ...
```

Momentan wissen wir, dass unsere Funktion irgendwas mit `print` heißen wird, irgendwelche Parameter bekommen wird,
etwas zurückgeben wird, und irgendeine Funktionalität implementiert.
Nichts davon ist bisher konkret.
Fangen wir also mit der Funktionalität an.
Wir haben bereits gelernt, dass wir mit `__dict__` uns unser Objekt als Dictionary repräsentieren können.
Also iterieren wir über unser Dictionary und geben uns einfach Schlüssel und Wert aus:

```python
class Settings:
    ...
    
    def print_(___) -> ____:
        for key, val in self.__dict__.items():
            print(f"\t{key}: {val}")
        return ...
```

Nun wissen wir auch, dass wir `self` als Parameter brauchen werden, und wir nichts zurück geben.

```python
class Settings:
    ...
    
    def print_(self) -> None:
        for key, val in self.__dict__.items():
            print(f"\t{key}: {val}")
```

Fertig ist die neue Funktion.
Müssen wir nun anmerken, dass es eine statische Methode ist?
Nein. Weil wir ja ein konkretes Objekt referenzieren.
Müssen wir anmerken, dass es eine Klasse ist?
Nein. Weil wir ja ein konkretes Objekt referenzieren.
Es ist eine vollwertige Methode.

Wenn wir zuerst die Funktion schreiben, danach die Parameter ergänzen und als letztes Überprüfen,
ob wir die Methode dekorieren müssen, finden wir immer heraus, welcher Decorator für eine Methode benötigt wird.

Nächstes Kapitel schauen wir uns an, wie wir die Einstellungen von der App aus ändern können,
damit wir nicht jedes mal die JSON zu öffnen müssen.
