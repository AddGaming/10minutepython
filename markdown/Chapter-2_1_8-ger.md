# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

### Kapitel 2.1.7: Try, Except, Try, Except, Try, ...

Das erste was mir einfällt ist, dass wir unser Schema nutzen sollten, um sicherzugehen,
dass alles richtig eingetragen worden ist:

```python
class Settings:
    ...

    @classmethod
    def load_from_file(cls):
        """
        loads the settings from the save file
        :return: Settings
        """
        path = os.sep.join(__file__.split(os.sep)[:-1])
        with open(f'{path}{os.sep}settings.json', "r") as f:
            content = json.loads(f.read())
        try:
            validate(content, cls._schema())
        except exceptions.ValidationError:
            print("settings.json is invalid\n\t-> instantiating a new version...")
            c = cls()
            c.save_to_file()
            return c
        return cls(
            default_source=content['default_source'],
            default_destination=content['default_destination'],
            file_format_list=content['file_format_list']
        )
```

Wir versuchen nun in einem `try` (de. versuchen) `except` (de. ausgenommen) Block unsere `validate()` Funktion
aufzurufen.
Als Parameter geben wir sowohl die aktuell eingelesene Datei, als auch das Schema mit.
Schlägt die Validierung fehl, gibt sie uns einen Fehler, welchen wir im `except` abfangen und bearbeiten können.
Fliegt kein Fehler, läuft das Programm einfach weiter und wir bauen ein neues Objekt mit den Parametern aus der
nun validierten JSON.
Erhalten wir einen `ValidationError`, machen wir nun 4 Sachen.
Als erstes geben wir in der ersten Zeile eine Nachricht an den Nutzer, dass die `settings.json` kaput ist.
Danach erstellen wir ein neues Objekt der Klasse `Settings`.
Nun speichern wir unser neues Objekt und legen damit eine korrekte Variante ab.
Und als letztes geben wir unser neues Objekt als Funktionsergebnis zurück.

Wir haben nun also erfolgreich den Fall abgefangen, dass wir eine Datei mit fehlerhaften Werten haben,
aber was, wenn erst gar keine Datei existiert?
Was machen wir, wenn das Programm gerade zum ersten mal ausgeführt wird?
Nun, dann wird uns Python einen `IOError` geben und sagen, dass es die Datei die wir suchen, nicht finden konnte.
Mit diesem Fehler können wir genauso umgehen, wie mit dem davor: try and except.

```python
class Settings:
    ...

    @classmethod
    def load_from_file(cls):
        """
        loads the settings from the save file
        :return: Settings
        """
        path = os.sep.join(__file__.split(os.sep)[:-1])
        try:
            with open(f'{path}{os.sep}settings.json', "r") as f:
                content = json.loads(f.read())
        except IOError:
            print("No settings found\n\t-> booting from blank defaults...")
            c = cls()
            c.save_to_file()
            return c
        try:
            validate(content, cls._schema())
        except exceptions.ValidationError:
            print("settings.json is invalid\n\t-> instantiating a new version...")
            c = cls()
            c.save_to_file()
            return c
        return cls(
            default_source=content['default_source'],
            default_destination=content['default_destination'],
            file_format_list=content['file_format_list']
        )
```

Gehen wir des Verständnisses halber noch einmal über diesen try-except Block drüber:
Innerhalb des Try-Blocks versuchen wir unsere Datei zu öffnen und auszulesen.
Sollte es dort eine Datei finden, gehen wir zum nächsten Code und überspringen den Except-Block.
Sollte die Datei nicht da sein, fangen wir den `IOError` ab.
Um das Problem zu lösen, sagen wir dem Nutzer, dass wir eins hatten,
erstellen ein neues Objekt unserer Klasse, speichern dieses
und geben dann das Objekt als Rückgabewert der Funktion zurück.

Nun haben wir schon die zwei häufigsten Fehler abfangen können.
Aber es gibt noch andere Wege, we unser Programm abstürzen würde.
Wenn die Datei nur zufällige Zeichen enthält (Bsp: "aosidjauowbzufbapowk"),
dann wir Python das nicht als JSON interpretieren können und wir erhalten einen
`json.decoder.JSONDecodeError`.
Oder, wenn die JSON leer ist. Dann wird sie auch die Validierung bestehen,
aber wir werden die Zuweisungen beim erstellen des neuen Objektes nicht durchführen können.
Um das zu verhindern können wir unser Schema anpassen:

```python
class Settings:
    ...

    @staticmethod
    def _schema() -> dict:
        """
        The validation schema
        :return: Json-Validation-Schema
        """
        return {
            "type": "object",
            "properties": {
                "default_source": {"type": "string"},
                "default_destination": {"type": "string"},
                "file_format_list": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                }
            },
            "required": [
                "default_source",
                "default_destination",
                "file_format_list"
            ]
        }
```

Jetzt hat unser Schema eine Liste an benötigten (eng. required) Feldern bekommen.
Wenn ein Feld mit dem Namen fehlt, gilt die JSON als ungültig.
In unserem fall brauchen wir "default_source", "default_destination", und "file_format_list".

Beheben wir nun auch den bereits angesprochenen `JSONDecoderError`.

```python
class Settings:
    ...

    @classmethod
    def load_from_file(cls) -> Settings:
        """
        loads the settings from the save file
        :return: Settings
        """
        path = os.sep.join(__file__.split(os.sep)[:-1])
        try:
            with open(f'{path}{os.sep}settings.json', "r") as f:
                # no validation needed, because unaccepted input_ is caught in loads() and __init__()
                content = json.loads(f.read())
        except IOError:
            print("No settings found\n\t-> booting from blank defaults...")
            c = cls()
            c.save_to_file()
            return c
        except json.decoder.JSONDecodeError:
            print("settings.json is corrupt\n\t-> instantiating a new version...")
            c = cls()
            c.save_to_file()
            return c
        try:
            validate(content, cls._schema())
        except exceptions.ValidationError:
            print("settings.json is invalid\n\t-> instantiating a new version...")
            c = cls()
            c.save_to_file()
            return c
        return cls(
            default_source=content['default_source'],
            default_destination=content['default_destination'],
            file_format_list=content['file_format_list']
        )
```

Wir müssen dafür keinen neuen Try-Block schreiben, da der Fehler bereits in einem Try-Block austritt.
Stattdessen können wir einen weiteren Except-Block anhängen.
Der Inhalt ist inhaltlich der Gleiche, wie in dem Block darüber.
Es wird lediglich eine neue Fehlermeldung zurückgegeben.

Machen wir noch eine Sache, bevor wir mit diesem Kapitel fertig sind.
Es fällt auf, dass wir auch hier wiederholt das selbe machen.
Jedes mal wenn unsere Datei kaputt ist, oder fehlt, machen wir das gleiche:

```python
 c = cls()
c.save_to_file()
return c
```

Also machen wir daraus eine Funktion und benutzen sie stattdessen:

```python
class Settings:
    ...
    
    @classmethod
    def __new_save(cls) -> Settings:
        """
        Saves a new settings.json
        :return: the new Settings object
        """
        c = cls()
        c.save_to_file()
        return c

    @classmethod
    def load_from_file(cls) -> Settings:
        """
        loads the settings from the save file
        :return: Settings
        """
        path = os.sep.join(__file__.split(os.sep)[:-1])
        try:
            with open(f'{path}{os.sep}settings.json', "r") as f:
                # no validation needed, because unaccepted input is caught in loads() and __init__()
                content = json.loads(f.read())
        except IOError:
            print("No settings found\n\t-> booting from blank defaults...")
            return cls.__new_save()
        except json.decoder.JSONDecodeError:
            print("settings.json is corrupt\n\t-> instantiating a new version...")
            return cls.__new_save()
        try:
            validate(content, cls._schema())
        except exceptions.ValidationError:
            print("settings.json is invalid\n\t-> instantiating a new version...")
            return cls.__new_save()
        return cls(
            default_source=content['default_source'],
            default_destination=content['default_destination'],
            file_format_list=content['file_format_list']
        )
```

Das wars auch bereits. Wir können nun sicher Dateien schreiben und einlesen.
In dem nächsten Kapitel gehe ich darauf ein, was es mit dem `@classmethod` und dem `@staticmethod` auf sich hat.

**Settings.py**

```python
"""
Settings Class for settings of Application
"""
from __future__ import annotations

import json
import os

from jsonschema import validate, exceptions


class Settings:
    """
    Class for handling user set settings management
    """
    default_source: str
    default_destination: str
    file_format_list: [str]

    def __init__(self, default_source: str = "", default_destination: str = "", file_format_list: [str] = None):
        self.default_source = default_source
        self.file_format_list = file_format_list
        self.default_destination = default_destination

    @staticmethod
    def _schema() -> dict:
        """
        The validation schema
        :return: Json-Validation-Schema
        """
        return {
            "type": "object",
            "properties": {
                "default_source": {"type": "string"},
                "default_destination": {"type": "string"},
                "file_format_list": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                }
            },
            "required": [
                "default_source",
                "default_destination",
                "file_format_list"
            ]
        }

    @classmethod
    def __new_save(cls) -> Settings:
        """
        Saves a new settings.json
        :return: the new Settings object
        """
        c = cls()
        c.save_to_file()
        return c

    @classmethod
    def load_from_file(cls) -> Settings:
        """
        loads the settings from the save file
        :return: Settings
        """
        path = os.sep.join(__file__.split(os.sep)[:-1])
        try:
            with open(f'{path}{os.sep}settings.json', "r") as f:
                # no validation needed, because unaccepted input is caught in loads() and __init__()
                content = json.loads(f.read())
        except IOError:
            print("No settings found\n\t-> booting from blank defaults...")
            return cls.__new_save()
        except json.decoder.JSONDecodeError:
            print("settings.json is corrupt\n\t-> instantiating a new version...")
            return cls.__new_save()
        try:
            validate(content, cls._schema())
        except exceptions.ValidationError:
            print("settings.json is invalid\n\t-> instantiating a new version...")
            return cls.__new_save()
        return cls(
            default_source=content['default_source'],
            default_destination=content['default_destination'],
            file_format_list=content['file_format_list']
        )

    def save_to_file(self) -> None:
        """
        saves the settings to the save file
        :return: None
        """
        dump = json.dumps(self.__dict__)
        path = os.sep.join(__file__.split(os.sep)[:-1])
        with open(f'{path}{os.sep}settings.json', "w") as f:
            f.write(dump)
```