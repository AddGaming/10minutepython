# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

### Kapitel 2.1.7: JSON

Wir sind nun mit folgendem Problem konfrontiert.
Wenn wir versuchen unsere Applikation zu nutzen, erhält Python als drittes Argument immer einen String,
welcher unsere Logik kaput macht.
Stattdessen benötigt unsere Eingabe eine gut formatierte Liste.
Aber wenn wir bei jedem Aufrufen der Funktion aufs neue unser Ziel eingeben müssten und unsere Herkunft
und unsere Dateitypen wird das Nutzen der Applikation sehr schnell sehr unangenehm.
Eine Eigenschaft, welche uns ermöglicht, diese Dinge einmalig zu hinterlegen, und nur wenn sich etwas ändert
diese erneut einzugeben, wäre nötig.
Genau das will ich heute mit Hilfe von JSON einführen.

JSON ist ein Dateiformat welches genauso strukturiert ist wie ein Python-Dictionary.
Wir haben schlüssel, welche jeweils einen Wert haben.
Python kann diese auch entsprechend einfach schreiben und einlesen:

```python
# schreiben
with open(f'{path}{os.sep}settings.json', "w") as f:
    f.write(dictionary)

# lesen
with open(f'{path}{os.sep}settings.json', "r") as f:
    content = json.loads(f.read())
```

Am besten lässt sich dieser Code verstehen, wenn wir ihn lesen, als wäre es normales Englisch.
Mit geöffneter `<Datei>` unter den Namen `<Namen>`: `<Name>`.schreib `<Inhalt>`.
Oder für das Konkrete Beispiel:
Mit geöffneten `Setting.json` unter den Namen `f`: `f`.schreib `dictionary`.
Analog gilt das gleiche für JSON lesen.
Wir öffnen eine Datei und geben ihr einen Namen, und danach benutzen wir diesen Namen in unseren Code.
Sobald unsere Einrückung aufhört, ist auch die Datei wieder zu und nicht mehr verfügbar.

Warum sollten wir nun diese Schreibweise mit dem `with` benutzen?
Es gibt auch die Alternative die Dateien manuell zu öffnen und manuell zu schließen.
Der Vorteil an der `with` schreibweise ist nun, dass man nicht vergessen kann Dateien zu schließen.
Außerdem spart man sich Fehler, weil man erwartet hat, dass die Datei noch geöffnet ist,
aber sie bereits geschlossen war.

Neben der einfachen Art und Weise JSON in Python zu schreiben und einzulesen,
gibt es noch den Vorteil, dass wir uns ein Schema spezifizieren können und damit unsere Eingabe validieren können.
Das ist immer dann wichtig, wenn wir Daten von Außen bekommen.
Dort treten dann oft die meisten Fehler auf.
Was, wenn die Datei nicht da ist? Was, wenn in einem Zahlenfeld ein Buchstabe steht?
All das können wir mit hilfe der Validierung abfangen und bearbeiten.
Das Schema als solches können wir einfach als Python Dictionary definieren:

```python
{
    "type": "object",
    "properties": {
        "default_source": {"type": "string"},
        "default_destination": {"type": "string"},
        "file_format_list": {
            "type": "array",
            "items": {
                "type": "string"
            }
        }
    }
}
```

Wie genau JSON Schemas aufgebaut sind, ist nicht so wichtig.
Die grobe Funktionsweise sollte von diesem Codeschnipsel verstanden werden können.
Falls du als engagierter Leser nun doch wissen willst, wie Schema und Validator funktionieren,
gibt es unzählige Werkzeuge und Internetseiten, welche dir ermöglichen mit Schemata herumzuspielen.
(Bsp: [jsonschemavalidator](https://www.jsonschemavalidator.net/))

Fangen wir nun an mit unseren Neuen wissen eine Klasse `Settings` anzulegen, welche unsere Einstellungen verwalten soll.

```python
class Settings:
    """
    Class for handling user set settings management
    """
    default_source: str
    default_destination: str
    file_format_list: [str]

    def __init__(self, default_source: str = "", default_destination: str = "", file_format_list: [str] = None):
        self.default_source = default_source
        self.file_format_list = file_format_list
        self.default_destination = default_destination
```

Wie bereits in dem Kapitel über Klassen erwähnt, haben Klassen meistens eine `__init__` funktion,
mit welcher der Startzustand neuer Objekte festgelegt werden kann.
Schreiben wir nun zusätzlich eine Methode, welche den aktuellen Zustand speicher:

```python
class Settings:
    ...

    def save_to_file(self) -> None:
        """
        saves the settings to the save file
        :return: None
        """
        dump = json.dumps(self.__dict__)
        path = os.sep.join(__file__.split(os.sep)[:-1])
        with open(f'{path}{os.sep}settings.json', "w") as f:
            f.write(dump)
```

Nun, das war einfach.
Wir speichern einfach unsere neue Datei an dem selben Ort ab, wo auch diese Python Datei ist.
Das `self.__dict__` brauch aber eine Erklärung.
Klassen haben verschiedenste vordefinierte Funktionen, welche wir auf ihnen ausführen können.
Diese sind meist mit einem `__` am Anfang und am Ende des Funktionsnamens kenntlich gemacht.
Diese doppelten Unterstriche heißen auch Dunder, weshalb ich sie mit dem Namen Dunder-Methode (eng. dunder method)
ansprechen werde.
Eine der Dunder-Methoden, welche uns zur verfügung steht, ist die `__dict__` Methode.
Diese gibt uns alle, der Klasse zugeordneten Variablen, als Dictionary zurück.

```python
>> >

class bsp:


    ...


def __init__(self):


    ...
self.a = 1
...
self.b = "hi"
...
self.c = ["mom", "!"]
...
>> > test = bsp()
>> > test.__dict__
{'a': 1, 'b': 'hi', 'c': ['mom', '!']}
```

Dies ermöglicht uns diesen sehr eleganten 4 Zeiler, welchen wir theoretisch noch kürzer schreiben könnten,
aber das wiederrum auf Kosten der Lesbarkeit passieren würde.

```python
# Das aufteilen in mehrere Zeilen bietet hier bessere Lesbarkeit
def save_to_file(self) -> None:
    with open(f'{os.sep.join(__file__.split(os.sep)[:-1])}{os.sep}settings.json', "w") as f:
        f.write(json.dumps(self.__dict__))
```

Ich hoffe, dass mir alle Leser zustimmen, wenn ich sage, dass die obere Variante besser lesbar und verständlich war.

Ok, nun können wir den aktuellen Stand speichern, aber was ist mit einlesen?

```python
class Settings:
    ...

    @classmethod
    def load_from_file(cls):
        """
        loads the settings from the save file
        :return: Settings
        """
        path = os.sep.join(__file__.split(os.sep)[:-1])
        with open(f'{path}{os.sep}settings.json', "r") as f:
            content = json.loads(f.read())
        return cls(
            default_source=content['default_source'],
            default_destination=content['default_destination'],
            file_format_list=content['file_format_list']
        )
```

Nun, das wäre definitive eine Variante, aber eine, die unser Programm schnell abstürzen lässt.
Also, gehen wir im nächsten Kapitel der reihe nach durch, was alles schiefgehen kann (und wahrscheinlich auch wird),
und bauen Gegenmaßnahmen ein.
Ich hoffe dass du es mir nicht übel nimmst, dass ich dich mit so vielen Fragen aus diesem Kapitel entlasse,
verspreche aber, viele davon im nächsten zu beantworten.
Hier noch die Datei von Heute:

**settings.py**

```python
"""
Settings Class for settings of Application
"""
import json
import os

from jsonschema import validate, exceptions


class Settings:
    """
    Class for handling user set settings management
    """
    default_source: str
    default_destination: str
    file_format_list: [str]

    def __init__(self, default_source: str = "", default_destination: str = "", file_format_list: [str] = None):
        self.default_source = default_source
        self.file_format_list = file_format_list
        self.default_destination = default_destination

    @staticmethod
    def _schema() -> dict:
        """
        The validation schema
        :return: Json-Validation-Schema
        """
        return {
            "type": "object",
            "properties": {
                "default_source": {"type": "string"},
                "default_destination": {"type": "string"},
                "file_format_list": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                }
            }
        }
    
    def save_to_file(self) -> None:
        """
        saves the settings to the save file
        :return: None
        """
        dump = json.dumps(self.__dict__)
        path = os.sep.join(__file__.split(os.sep)[:-1])
        with open(f'{path}{os.sep}settings.json', "w") as f:
            f.write(dump)

    @classmethod
    def load_from_file(cls):
        """
        loads the settings from the save file
        :return: Settings
        """
        path = os.sep.join(__file__.split(os.sep)[:-1])
        with open(f'{path}{os.sep}settings.json', "r") as f:
            content = json.loads(f.read())
        return cls(
            default_source=content['default_source'],
            default_destination=content['default_destination'],
            file_format_list=content['file_format_list']
        )
```