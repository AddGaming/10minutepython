# Kapitel 2: Real live apps

## Kapitel 2.1: Sauber Importieren von Files

### Kapitel 2.1.2: Py.Os

Wenn wir Dateien bewegen und Manipulieren wollen, kommen wir nicht darum,
mit dem Operating System (kurz OS) zu interagieren.
Python bietet uns dafür die bereits implementierte Bibliothek/Paket `os`.

Wenn wir diese nutzen wollen, fügen wir einfach ein `import os` oberhalb unseres Codes ein.

Das erste, was wir mit der neuen Bibliothek machen werden, ist unseren Ausführungspfad anpassen.
Standardmäßig führen wir unsere Programme aus dem Ordner aus, indem wir sie abgespeichert haben.
Das ist aber nicht immer der Fall.
Um also sicher zu stellen, dass wir uns immer genau dort im Dateisystem befinden, wovon wir ausgehen,
ändern wir sicherheitshalber das aktuelle Ausführungsverzeichnis zu Begin des Programmes.
Zusätzlich zeigt uns die dafür benötigte Zeile Code auch schon 2 Funktionalitäten der `os`-Bibliothek.

> **Einschub: Was ist der Unterschied zwischen Bibliothek und Paket?**
> In Python beschreiben beide Namen, das selbe Verhalten für den Endnutzer.
> Dabei ist jede Bibliothek ein Paket.
> Der Unterschied ist jedoch, dass wir Bibliotheken Pakete von außerhalb des Projektes sind.
> Heißt, wenn wir das `os`-Paket nutzen, benutzen wir die Bibliothek `os`.
> Wenn wir jedoch ein eigenes Paket neu schreiben innerhalb dieses Projektes, wird es nicht automatisch zur Bibliothek.
> Das Entwickeln einer Bibliothek ist meistens sehr Arbeitsaufwändig und deshalb wird Anfängern zwar davon abgeraten,
> eigene Bibliotheken zu verfassen, aber angehalten, sich den Code in diesen anzuschauen um zu lernen,
> da Code in Bibliotheken meist einen hohen Standard hat.

Die Zeile die wir uns in dem heutigen Teil anschauen ist:

```python
os.chdir(os.sep.join(__file__.split(os.sep)[:-1]))
```

Jep, das ist alles.
Gleichzeitig steckt schon ganz schön viel in dieser Zeile.
Nehmen wir sie also Stück für Stück auseinander.

Der innerste Teil des Funktion ist `__file__`.
Was genau File macht, wissen wir bereits.
Dies ist eine Vordefinierte Variabel, welche uns den genauen Speicherort der Datei wiedergibt.

Als nächstes führen wir auf diesem Pfad die Operation `.split()` aus mit dem separierenden Zeichen `os.sep`.
Was genau `.split()` macht, wissen wir auch bereits.
`os.sep` gibt uns das Trennzeichen für Dateipfade auf unserem aktuellen Betriebssystem.
Diese Unterscheiden sich zwischen Windows, Linux und Apple.
Deshalb sollte man sich angewöhnen, nicht `.split("\\")` zu schreiben, was nur unter Windows funktioniert,
sondern stattdessen `.split(os.sep)`, was auf allen Betriebssystemen funktioniert.

Als letztes führen wir auf der Liste, die wir dadurch bekommen eine Selektierung aus.
`[:-1]` gibt uns dabei alle Elemente mit ausnahme des Letzten.
Aber wie genau setzt sich `[:-1]` zusammen?
Nun, wir wissen bereits, dass man mit `meine_liste[0]` das erste Element einer Liste erhält.
Mit `meine_liste[1]` das Zweite und mit `meine_liste[2]` das Dritte.
Nun unterstützt Python aber auch, dass wir eine Liste rückwärts durchsuchen.
Mit `meine_liste[-1]` erhalten wir das erste Element von hinten.
Mit `meine_liste[-2]` das zweit Letzte und mit `meine_liste[-3]` das dritt Letzte.
Ok, das erklärt die `-1` in `[:-1]`, aber was macht der `:` da?
Mit dem Doppelpunkt können wir uns einen Teil der Liste aus der Ursprünglichen ausschneiden.
Beispielsweise gibt uns `meine_liste[2:4]` die Elemente von der dritten Position bis inklusive der vierten Position.
Bsp:

```python
>>> my_list = [0, 1, 2, 3, 4]
>>> my_list[2:4]
[2, 3]
```

Als letztes Puzzlestück fehlt noch, was wir bei dem `:` schreiben müssen, wenn wir das erste,
oder das letzte Element haben wollen.
Normalerweise würde man `meine_liste[0:-1]` erwarten.
Aber damit holen haben wir Leider zwar das erste Element drinnen, aber nicht das Letzte.

```python
>>> my_list[0:-1]
[0, 1, 2, 3]
```

Aus diesem Grund lassen wir per Konvention entweder den vorderen Teil frei, wenn wir ab dem ersten Element an arbeiten,
und den hinteren Teil, wenn wir bis zum letzten Element arbeiten.

```python
>>> my_list[:-1]
[0, 1, 2, 3]
>>> my_list[1:]
[1, 2, 3, 4]
```

Ok. Da also zusammenfassend sorgt `__file__.split(os.sep)[:-1]` dafür, dass wir den Pfad der Datei nehmen,
diesen in eine Liste seiner Einzelteile unterteilen, und schließlich das letzte Element dieser Liste wegwerfen.
Somit erhalten wir den Ordner, an dem sich das Programm befindet.

Nun ist der Pfad jedoch kein String mehr, sondern eine Liste.
Aber wir wissen bereits, wie man aus einer Liste wieder einen Text macht; mit `.join()`
Dafür verknüpfen wir einfach den Pfad wieder mit dem Zeichen, mit dem wir ihn vorher getrennt hatten (`os.sep`).
Somit erhalten wir `os.sep.join(__file__.split(os.sep)[:-1])`

Damit sind wir fast am Ende.
Alles was jetzt noch zu tun ist, ist das aktuelle Arbeitsverzeichnis zu dem Pfad zu machen,
den wir uns erarbeitet haben.
Das machen wir mit `os.chdir()`. Dabei steht die Abkürzung für ***Ch**ange **Dir**ectory*.
Wir sind wieder da, wo wir heute gestartet sind: `os.chdir(os.sep.join(__file__.split(os.sep)[:-1]))`

Und das wars auch für heute schon.
Falls das Teilen und wieder Zusammenfügen mit `.join()` und `.split()` noch etwas ungewohnt war,
oder viel mentale Anstrengung gebraucht hat, ist das nicht weiter schlimm, sollte aber ein Zeichen sein,
vielleicht noch einmal selbst mit beidem zu Spielen, um mit der Funktionsweise vertraut zu sein.
Beides wird relativ häufig benutzt und ich werde auf zukünftiges Auftreten beider Funktionen
kein besonderes Augenmark mehr legen.

**main.py**

```python
"""
Starting Point of the CLI
Programm to import and de-duplicate files from an external device
"""

if __name__ == "__main__":
    os.chdir(os.sep.join(__file__.split(os.sep)[:-1]))
    
    while True:
        cmd_param = input("$: ")
        if cmd_param.split()[0] == "quit":
            quit()
        else:
            print(f"unrecognized input: {cmd_param.split()[0]}")
```
