# Kapitel 1: Grundlagen

## Kapitel 1.1: Hi Mom 

Alles Anfang ist schwer.     
Daher werde ich nicht direkt mit einem komplexeren Programm anfangen, was verwendung im Alltag finden kann,    
sondern werde Stück für Stück die nötigen Grundbausteine einführen.   

Im Fall von Heute; das erste Programm schreiben.   

Ich werde im folgenden die Programmiersprache Python benutzen.   
Um genauer zu sein Python 3.  
Wenn man im Internet zu dem Thema Programmiersprachen recherchiert,  
wird man schnell Debatten um Vor- und Nachteile der einzelnen Programmiersprachen finden.  
Diese sind jedoch für uns größtenteils irrelevant.   

Was mir wichtig ist, dass die Sprache "einfach zu lesen" und "viele unterschiedliche Funktionen" unterstützt.   
Somit sollte es nicht nötig sein eine neue Programmiersprache zu lernen, nur weil man Projekt-x machen will.   
Gleichermaßen sollte "einfach zu lesen" dafür sorgen, dass nicht Notation eine Hürde im Lernprozess wird.   

Beginnen wir also mit der ersten Aufgabe:   
> **Aufgabe**    
> Installiert die aktuellste version von Python auf euren PC.   
> https://www.python.org/downloads/   

Mit der URL oben kommt ihr zu den offiziellen Downloadlinks.    
Ein großer gelber Button sollte euch entgegenspringen.   

Nachdem ihr Python Installiert habt, müssen wir überprüfen,   
ob alles richtig geklappt hat.   

Dafür öffnen wir die Kommandokonsole.    
Ich werde dieses Wort ab sofort mit "Consol" abkürzen.   
Ihr öffnet die Consol unter Windows indem ihr in die Suchleiste "CMD" eingebt.   
Nun dürftet ihr mit diesem Text begrüßt werden:   

```
Microsoft Windows [Version 10.0.19043.2006]
(c) Microsoft Corporation. Alle Rechte vorbehalten.

C:\Users\your_user_name>
```

Der erste Teil (`C:\Users\your_user_name`) ist der Ort, an dem ihr euch auf eurem PC befindet.   
Dies wird aber erst ein anderes Mal wichtig werden.   
Der zweite Teil ist Alles, was ihr hinter dem `>` eingebt.   

Dies sind Befehle, welche von eurer Consol interpretiert werden.   
Für den Anfang führen wir nun den Befehl `py` in unserer Consol aus.   

```
C:\Users\your_user_name>py
Python 3.10.2 (tags/v3.10.2:a58ebcc, Jan 17 2022, 14:12:15) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

> *An der Stelle bin ich wohl verpflichtet anzumerken,*    
> *dass dieser Befehl bei MacOS oder Linux nicht unbedingt funktioniert.*  
> *Öffnet bei MacOS das Launchpad und sucht nach dem Terminal.*     
> *Dort sollte der Befehl "python" oder "python3" funktionieren.*      
> *Linux nutzer sind wahrscheinlich selbstständig genug es selbst rauszufinden.*   

Aber zurück zu Python in unserer Consol.   
Die ersten beiden Zeilen zeigen uns Informationen über die Python version die wir verwenden.   
Also kann diese im Moment ignoriert werden.    
Hinter den `>>>` können wir nun Pythonbefehle schreiben, welche nach dem Bestätigen mit der `Enter` Taste, ausgeführt werden.   

Schreiben wir also unseren ersten 1-Zeiler:   

```python
print("Hi Mom")
```

Wenn wir die Zeile ausführen, erhalten wir den Text in den `"` als Antwort auf der Consol.   

```python
>>> print("Hi Mom")
Hi Mom
```

`print` ist hierbei eine Funktion die aufgerufen wird.   
Ihre Aufgabe ist einfach: Den Text auf der Consol ausgeben.   

Und das wars auch schon für Heute. Ein letzte Aufgabe habe ich aber noch.   
>**Aufgabe:**   
>Probiert ein bisschen selbst rum und schreibt Text in die Consol.   
>Denkt dabei immer an die `"` vor und hinter eurem Text, da es ansonsten Fehler geben wird.   

Wenn ihr fertig seid, könnt ihr das Konsolenfenster einfach schließen,    
oder mit `quit()` python beenden und euch noch etwas in der Consol umschauen.   
